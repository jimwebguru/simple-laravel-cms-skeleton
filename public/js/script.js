$(document).ready(function ()
{
    $('.hero-slider').bxSlider({auto: true, pause: 6000, stopAutoOnClick: true, responsive: true});
});

function isMobile()
{
    var ua = navigator.userAgent;

    return (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini|Mobile|mobile|CriOS/i.test(ua));
}