<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Blog extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "blogs";

    public function getDates()
    {
        return ['created_at', 'updated_at', 'published_date'];
    }
}
