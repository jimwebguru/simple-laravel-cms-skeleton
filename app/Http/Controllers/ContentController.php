<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Content;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Session;

class ContentController extends Controller
{
    private $rules = array(
            'name'       => 'required',
            'body'      => 'required'
        );

    public function __construct()
    {
        $this->middleware('auth',['except' => 'content']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = array();
        $data["items"] = Content::all();

        return view('content.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('content.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(Input::all(), $this->rules);

        // process the login
        if ($validator->fails())
        {
            return Redirect::to('contents/create')
                ->withErrors($validator)
                ->withInput();
        }
        else
        {
            // store
            $item = new Content();
            $item->name = $request->input('name');
            $item->body = $request->input('body');
            $item->url_path = $request->input('url_path');
            $item->block_url = $request->input('block_url');
            $item->is_code = $request->input('is_code');
            $item->save();

            // redirect
            Session::flash('message', 'Successfully created content!');

            return Redirect::to('contents');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['item'] = Content::find($id);

        return view('content.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['item'] = Content::find($id);

        return view('content.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make(Input::all(), $this->rules);

        // process the login
        if ($validator->fails())
        {
            return Redirect::to('contents/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput();
        }
        else
        {
            // store
            $item = Content::find($id);
            $item->name = $request->input('name');
            $item->body = $request->input('body');
            $item->url_path = $request->input('url_path');
            $item->block_url = $request->input('block_url');
            $item->is_code = $request->input('is_code');
            $item->save();

            // redirect
            Session::flash('message', 'Successfully updated content!');

            return Redirect::to('contents');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Content::find($id);
        $item->delete();

        // redirect
        Session::flash('message', 'Successfully deleted!');

        return Redirect::to('contents');

    }

    public function cloneContent($id)
    {
        $item = Content::find($id);

        $newItem = $item->replicate();
        $newItem->id = 0;
        $newItem->name = $item->name . " (CLONE)";
        $newItem->save();

        // redirect
        Session::flash('message', "Content $id cloned!");

        return Redirect::to('contents');
    }

    public function content($urlAlias)
    {
        $contents = Content::where('url_path','=', $urlAlias)->get();
        $blocks = Content::where('block_url','=', $urlAlias)->get();

        $data['content'] = $contents[0];
        $data['blocks'] = $blocks;

        return view('content.content', $data);
    }
}
