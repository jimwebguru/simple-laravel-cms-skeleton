<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;
use App\MenuItem;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Session;

class MenuController extends Controller
{
    private $rules = array(
            'name'       => 'required',
            'token_name' => 'required'
        );

    private $menuItemRules = array(
        'url' => 'required',
        'link_text' => 'required'
    );

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = array();
        $data["items"] = Menu::all();

        return view('menu.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('menu.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(Input::all(), $this->rules);

        // process the login
        if ($validator->fails())
        {
            return Redirect::to('menus/create')
                ->withErrors($validator)
                ->withInput();
        }
        else
        {
            // store
            $item = new Menu();
            $this->saveMenu($item, $request);

            // redirect
            Session::flash('message', 'Successfully created menu!');

            return Redirect::to('menus/'.$item->id.'/edit');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['item'] = Menu::find($id);

        return view('menu.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['item'] = Menu::find($id);

        return view('menu.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make(Input::all(), $this->rules);

        // process the login
        if ($validator->fails())
        {
            return Redirect::to('menus/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput();
        }
        else
        {
            // store
            $item = Menu::find($id);
            $this->saveMenu($item, $request);

            // redirect
            Session::flash('message', 'Successfully updated menu!');

            return Redirect::to('menus/'.$item->id.'/edit');
        }
    }

    private function saveMenu($item, $request)
    {
        $item->name = $request->input('name');
        $item->token_name = $request->input('token_name');
        $item->menu_css = $request->input('menu_css');
        $item->aligned_right = $request->input('aligned_right');

        $item->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Menu::find($id);

        if($item->menuItems->count() > 0)
        {
            foreach($item->menuItems as $subMenuItem)
            {
                $this->deleteSubMenuItems($subMenuItem);
            }
        }

        $item->delete();

        // redirect
        Session::flash('message', 'Successfully deleted!');

        return Redirect::to('menus');

    }

    public function cloneMenu($id)
    {
        $item = Menu::find($id);

        $newItem = $item->replicate();
        $newItem->id = 0;
        $newItem->name = $item->name . " (CLONE)";
        $newItem->save();

        // redirect
        Session::flash('message', "Menu $id cloned!");

        return Redirect::to('menus');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createMenuItem($id, $parentType)
    {
        $data = array();

        if($parentType == 0)
        {
            $data['menu_id'] = $id;
        }
        else
        {
            $data['parent_menuitem_id'] = $id;
        }

        return view('menu.create_menuitem', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeMenuItem(Request $request)
    {
        $validator = Validator::make(Input::all(), $this->menuItemRules);

        $parent = null;

        if($request->input('menu_id') != 0)
        {
            $parent = Menu::find($request->input('menu_id'));
        }
        else
        {
            $parent = MenuItem::find($request->input('parent_menuitem_id'));
        }

        // process the login
        if ($validator->fails())
        {
            return Redirect::to('menus/create-menuitem/'.$parent->id.'/'.(($parent instanceof Menu)? '0': '1'))
                ->withErrors($validator)
                ->withInput();
        }
        else
        {
            // store
            $item = new MenuItem();

            $item->weight = $parent->menuItems->count();

            $this->saveMenuItem($item, $request);

            // redirect
            Session::flash('message', 'Successfully created menu item!');

            if($parent instanceof MenuItem)
            {
                return Redirect::to('menus/edit-menuitem/'.$item->parent_menuitem_id);
            }

            return Redirect::to('menus/'.$request->input('menu_id').'/edit');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editMenuItem($id)
    {
        $data['item'] = MenuItem::find($id);

        return view('menu.edit_menuitem', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateMenuItem(Request $request)
    {
        $validator = Validator::make(Input::all(), $this->menuItemRules);

        if ($validator->fails())
        {
            return Redirect::to('menus/edit-menuitem/'.$request->input('id'))
                ->withErrors($validator)
                ->withInput();
        }
        else
        {
            // store
            $item = MenuItem::find($request->input('id'));
            $this->saveMenuItem($item, $request);

            // redirect
            Session::flash('message', 'Successfully updated menu item!');

            $parent = null;

            if($item->menu_id != 0)
            {
                $parent = Menu::find($item->menu_id);
            }
            else
            {
                $parent = MenuItem::find($item->parent_menuitem_id);
            }

            if($parent instanceof MenuItem)
            {
                return Redirect::to('menus/edit-menuitem/'.$item->parent_menuitem_id);
            }

            return Redirect::to('menus/'.$request->input('menu_id').'/edit');
        }
    }

    private function saveMenuItem($item, $request)
    {
        $item->url = $request->input('url');
        $item->link_text = $request->input('link_text');
        $item->menu_id = $request->input('menu_id');
        $item->parent_menuitem_id = $request->input('parent_menuitem_id');
        $item->auth_required = $request->input('auth_required');

        $item->save();
    }

    public function deleteMenuItem($id)
    {
        $item = MenuItem::find($id);

        if($item->menuItems->count() > 0)
        {
            foreach($item->menuItems as $subMenuItem)
            {
                $this->deleteSubMenuItems($subMenuItem);
            }
        }

        $item->delete();

        $parent = null;

        if($item->menu_id != 0)
        {
            $parent = Menu::find($item->menu_id);
        }
        else
        {
            $parent = MenuItem::find($item->parent_menuitem_id);
        }

        Session::flash('message', 'Deleted menu item!');

        if($parent instanceof MenuItem)
        {
            return Redirect::to('menus/edit-menuitem/'.$item->parent_menuitem_id);
        }

        return Redirect::to('menus/'.$item->menu_id.'/edit');
    }

    private function deleteSubMenuItems($menuItem)
    {
        if($menuItem->menuItems->count() > 0)
        {
            foreach($menuItem->menuItems as $subMenuItem)
            {
                $this->deleteSubMenuItems($subMenuItem);
            }
        }

        $menuItem->delete();
    }

    public function setMenuItemWeight($id, $weight)
    {
        $menuItem = MenuItem::find($id);
        $parent = null;

        if($menuItem->menu_id != 0)
        {
            $parent = Menu::find($menuItem->menu_id);
        }
        else
        {
            $parent = MenuItem::find($menuItem->parent_menuitem_id);
        }

        $otherMenuItems = $parent->menuItems->keyBy('id')->forget($menuItem->id);

        $menuItem->weight = $weight;
        $menuItem->save();

        $index = 0;

        foreach($otherMenuItems as $oMenuItem)
        {
            if($index == $weight)
            {
                $index++;
            }

            $oMenuItem->weight = $index;
            $oMenuItem->save();

            $index++;
        }
    }

    public function preview($id)
    {
        $menu = Menu::find($id);

        $data['tokenName'] = $menu->token_name;

        return view('menu.preview', $data);
    }
}
