<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    public $maxAttempts = 5;
    public $decayMinutes = 1440;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm(Request $request)
    {
        if($this->hasTooManyLoginAttempts($request))
        {
            $seconds = $this->limiter()->availableIn(
                $this->throttleKey($request)
            );

            $data["message"] = "TOO MANY FAILED LOGIN ATTEMPTS. Please try again in ".$this->secToHR($seconds);

            return view('auth.throttle', $data);
        }

        return view('auth.login');
    }


    /**
     * Get the throttle key for the given request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     *
     * THIS IS FOR CHANGING THE THROTTLE KEY TO IP BASED
     * REMOVE THIS FUNCTION IF YOU WANT TO USE THE OUT OF THE BOX ONE
     * WHICH USES USERNAME AND IP
     */
    protected function throttleKey(Request $request)
    {
        if($request->ip() == null || trim($request->ip()) == "")
        {
            return Str::lower($request->input($this->username())).'|'.$request->ip();
        }

        return $request->ip();
    }

    function secToHR($sec)
    {
            $hours = floor($sec / 3600);
            $minutes = floor(($sec / 60) % 60);
            $secs = $sec % 60;

            return $hours > 0 ? "$hours hours, $minutes minutes" : ($minutes > 0 ? "$minutes minutes, $secs seconds" : "$secs seconds");
    }
}
