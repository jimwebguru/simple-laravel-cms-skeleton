<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FAQ;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Session;

class FaqController extends Controller
{
    private $rules = array(
            'question'       => 'required',
            'answer'      => 'required'
        );

    public function __construct()
    {
        $this->middleware('auth',['except' => 'faqList']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = array();
        $data["items"] = FAQ::all();

        return view('faq.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('faq.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(Input::all(), $this->rules);

        // process the login
        if ($validator->fails())
        {
            return Redirect::to('faqs/create')
                ->withErrors($validator)
                ->withInput();
        }
        else
        {
            // store
            $item = new FAQ();
            $item->question = $request->input('question');
            $item->answer = $request->input('answer');
            $item->save();

            // redirect
            Session::flash('message', 'Successfully created FAQ!');

            return Redirect::to('faqs');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['item'] = FAQ::find($id);

        return view('faq.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['item'] = FAQ::find($id);

        return view('faq.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make(Input::all(), $this->rules);

        // process the login
        if ($validator->fails())
        {
            return Redirect::to('faqs/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput();
        }
        else
        {
            // store
            $item = FAQ::find($id);
            $item->question = $request->input('question');
            $item->answer = $request->input('answer');
            $item->save();

            // redirect
            Session::flash('message', 'Successfully updated FAQ!');

            return Redirect::to('faqs');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = FAQ::find($id);
        $item->delete();

        // redirect
        Session::flash('message', 'Successfully deleted!');

        return Redirect::to('faqs');

    }

    public function faqList()
    {
        $faqs = FAQ::all();

        $data['items'] = $faqs;

        return view('faq.faq_list', $data);
    }
}
