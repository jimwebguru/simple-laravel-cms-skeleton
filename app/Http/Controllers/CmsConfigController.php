<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Session;
use File;

use App\CmsConfig;
use App\Image;

class CmsConfigController extends Controller
{
    private $rules = array(
        'config_type' => 'required',
        'value' => 'required'
    );

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        \Auth::user()->authorizeRoles(['admin']);

        $data = array();
        $data["items"] = CmsConfig::all();

        return view('settings.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        \Auth::user()->authorizeRoles(['admin']);

        return view('settings.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \Auth::user()->authorizeRoles(['admin']);

        $validator = Validator::make(Input::all(), $this->rules);

        // process the login
        if ($validator->fails())
        {
            return Redirect::to('settings/create')
                ->withErrors($validator)
                ->withInput();
        }
        else
        {
            // store
            $item = new CmsConfig();
            $item->config_type = $request->input('config_type');
            $item->value = $request->input('value');
            $item->locked = $request->input('locked');

            if($item->config_type == "image-library-path")
            {
                if(!File::isDirectory(public_path().'/'.$item->value))
                {
                    File::makedirectory(public_path().'/'.$item->value);
                }
            }

            $item->save();

            // redirect
            Session::flash('message', 'Successfully created setting!');

            return Redirect::to('settings');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        \Auth::user()->authorizeRoles(['admin']);

        $data['item'] = CmsConfig::find($id);

        return view('settings.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        \Auth::user()->authorizeRoles(['admin']);

        $data['item'] = CmsConfig::find($id);

        return view('settings.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        \Auth::user()->authorizeRoles(['admin']);

        $validator = Validator::make(Input::all(), $this->rules);

        // process the login
        if ($validator->fails())
        {
            return Redirect::to('settings/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput();
        }
        else
        {
            // store
            $item = CmsConfig::find($id);
            $item->config_type = $request->input('config_type');
            $item->value = $request->input('value');
            $item->locked = $request->input('locked');

            if($item->config_type == "image-library-path")
            {
                if(!File::isDirectory(public_path().'/'.$item->value))
                {
                    File::makedirectory(public_path().'/'.$item->value);
                }
            }

            $item->save();

            // redirect
            Session::flash('message', 'Successfully updated setting!');

            return Redirect::to('settings');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \Auth::user()->authorizeRoles(['admin']);

        $item = CmsConfig::find($id);

        if($item->locked == 1)
        {
            // redirect
            Session::flash('message', 'Setting is locked and cannot be deleted!');

            return Redirect::to('settings');
        }

        if($item->config_type == "image-library-path")
        {
            $images = Image::where('url','LIKE','/'.$item->value.'%')->get();

            foreach($images as $img)
            {
                $img->delete();
            }

            if(File::isDirectory(public_path().'/'.$item->value))
            {
                File::deleteDirectory(public_path().'/'.$item->value);
            }
        }

        $item->delete();

        // redirect
        Session::flash('message', 'Successfully deleted!');

        return Redirect::to('settings');

    }
}
