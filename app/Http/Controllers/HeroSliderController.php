<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HeroSlider;
use App\Slide;
use App\Image;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Session;

class HeroSliderController extends Controller
{
    private $rules = array(
            'name'       => 'required',
            'token_name' => 'required'
        );

    private $slideRules = array(
        'title'       => 'required'
    );

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = array();
        $data["items"] = HeroSlider::all();

        return view('heroslider.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('heroslider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(Input::all(), $this->rules);

        // process the login
        if ($validator->fails())
        {
            return Redirect::to('herosliders/create')
                ->withErrors($validator)
                ->withInput();
        }
        else
        {
            // store
            $item = new HeroSlider();
            $this->saveHeroSlider($item, $request);

            // redirect
            Session::flash('message', 'Successfully created hero slider!');

            return Redirect::to('herosliders/'.$item->id.'/edit');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['item'] = HeroSlider::find($id);

        return view('heroslider.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['item'] = HeroSlider::find($id);

        return view('heroslider.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make(Input::all(), $this->rules);

        // process the login
        if ($validator->fails())
        {
            return Redirect::to('herosliders/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput();
        }
        else
        {
            // store
            $item = HeroSlider::find($id);
            $this->saveHeroSlider($item, $request);

            // redirect
            Session::flash('message', 'Successfully updated hero slider!');

            return Redirect::to('herosliders/'.$item->id.'/edit');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = HeroSlider::find($id);
        $item->delete();

        // redirect
        Session::flash('message', 'Successfully deleted!');

        return Redirect::to('herosliders');

    }

    private function saveHeroSlider($item, $request)
    {
        $item->name = $request->input('name');
        $item->token_name = $request->input('token_name');
        $item->title_css = $request->input('title_css');
        $item->caption_css = $request->input('caption_css');
        $item->button_css = $request->input('button_css');
        $item->button_hover_css = $request->input('button_hover_css');

        $item->save();
    }

    public function cloneHeroSlider($id)
    {
        $item = HeroSlider::find($id);

        $newItem = $item->replicate();
        $newItem->id = 0;
        $newItem->name = $item->name . " (CLONE)";
        $newItem->save();

        // redirect
        Session::flash('message', "Hero slider $id cloned!");

        return Redirect::to('herosliders');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createSlide($id)
    {
        $data['hero_id'] = $id;

        return view('heroslider.create_slide', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeSlide(Request $request)
    {
        $validator = Validator::make(Input::all(), $this->slideRules);

        // process the login
        if ($validator->fails())
        {
            return Redirect::to('herosliders/'.$request->input('hero_id').'/create-slide')
                ->withErrors($validator)
                ->withInput();
        }
        else
        {
            // store
            $item = new Slide();

            $heroSlider = HeroSlider::find($request->input('hero_id'));

            $item->weight = $heroSlider->slides->count();

            $this->saveSlide($item, $request);

            // redirect
            Session::flash('message', 'Successfully created slide!');

            return Redirect::to('herosliders/'.$request->input('hero_id').'/edit');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editSlide($id)
    {
        $data['item'] = Slide::find($id);
        $data['hero_id'] = $data['item']->hero_id;

        return view('heroslider.edit_slide', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateSlide(Request $request)
    {
        $validator = Validator::make(Input::all(), $this->slideRules);

        if ($validator->fails())
        {
            return Redirect::to('herosliders/edit-slide/'.$request->input('id'))
                ->withErrors($validator)
                ->withInput();
        }
        else
        {
            // store
            $item = Slide::find($request->input('id'));
            $this->saveSlide($item, $request);

            // redirect
            Session::flash('message', 'Successfully updated slide!');

            return Redirect::to('herosliders/'.$request->input('hero_id').'/edit');
        }
    }

    private function saveSlide($item, $request)
    {
        $item->title = $request->input('title');
        $item->caption = $request->input('caption');
        $item->url = $request->input('url');
        $item->link_text = $request->input('link_text');
        $item->hero_id = $request->input('hero_id');

        if($request->file('slide_image_upload') !== null)
        {
            $folder = "images/slides";

            $slideImage = new Image();
            $slideImage->name = $request->file('slide_image_upload')->getClientOriginalName();

            $fileName = time().pathinfo($slideImage->name, PATHINFO_EXTENSION);

            $slideImage->url = '/'.$folder.'/'.$fileName;
            $item->image = $slideImage->url;
            $slideImage->locked = 1;

            $request->file('slide_image_upload')->move(public_path() . '/'.$folder, $fileName);

            //Hero slider images don't need to be saved to the image library for now
            //$slideImage->save();
        }

        $item->save();
    }

    public function deleteSlide($id)
    {
        $item = Slide::find($id);

        $item->delete();

        Session::flash('message', 'Deleted slide!');

        return Redirect::to('herosliders/'.$item->hero_id.'/edit');
    }

    public function setSlideWeight($id, $weight)
    {
        $slide = Slide::find($id);
        $heroSlider = HeroSlider::find($slide->hero_id);

        $otherSlides = $heroSlider->slides->keyBy('id')->forget($slide->id);

        $slide->weight = $weight;
        $slide->save();

        $index = 0;

        foreach($otherSlides as $hSlide)
        {
            if($index == $weight)
            {
                $index++;
            }

            $hSlide->weight = $index;
            $hSlide->save();

            $index++;
        }

        Session::flash('message', 'Slide shifted!');

        return Redirect::to('herosliders/'.$heroSlider->id.'/edit');
    }
}
