<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Session;

use App\User;
use App\Role;

class UserController extends Controller
{
    private $rules = array(
        'name'       => 'required',
        'email'      => 'required'
    );

    private $changePasswordRules = array(
        'password' => 'required|confirmed|min:6'
    );

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        \Auth::user()->authorizeRoles(['admin']);

        $data = array();
        $data["items"] = User::all();

        return view('user.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        \Auth::user()->authorizeRoles(['admin']);

        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \Auth::user()->authorizeRoles(['admin']);

        $validator = Validator::make(Input::all(), $this->rules);

        // process the login
        if ($validator->fails())
        {
            return Redirect::to('users/create')
                ->withErrors($validator)
                ->withInput();
        }
        else
        {
            // store
            $item = new User();
            $item->name = $request->input('name');
            $item->email = $request->input('email');
            $item->password = bcrypt($request->input('password'));

            $item->save();

            $item->roles()->attach(Role::where('name', 'user')->first());

            // redirect
            Session::flash('message', 'Successfully created user!');

            return Redirect::to('users');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['item'] = User::find($id);

        return view('user.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        \Auth::user()->authorizeRoles(['admin']);

        $data['item'] = User::find($id);

        return view('user.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        \Auth::user()->authorizeRoles(['admin']);

        $validator = Validator::make(Input::all(), $this->rules);

        // process the login
        if ($validator->fails())
        {
            return Redirect::to('users/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput();
        }
        else
        {
            // store
            $item = User::find($id);
            $item->name = $request->input('name');
            $item->email = $request->input('email');

            $item->save();

            // redirect
            Session::flash('message', 'Successfully updated user!');

            return Redirect::to('users');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \Auth::user()->authorizeRoles(['admin']);

        $item = User::find($id);
        $item->delete();

        // redirect
        Session::flash('message', 'Successfully deleted!');

        return Redirect::to('users');

    }

    public function changePassword($id)
    {
        \Auth::user()->authorizeRoles(['admin']);

        $data['item'] = User::find($id);

        return view('user.change_password', $data);
    }

    public function updatePassword(Request $request)
    {
        \Auth::user()->authorizeRoles(['admin']);

        $item = User::find($request->input('id'));

        $validator = Validator::make(Input::all(), $this->changePasswordRules);

        // process the login
        if ($validator->fails())
        {
            return Redirect::to('change-password/' . $item->id)
                ->withErrors($validator)
                ->withInput();
        }

        $item->password = bcrypt($request->input('password'));

        $item->save();

        Session::flash('message', 'Updated password for ' .$item->name. '!');

        return Redirect::to('users');
    }

    public function myProfile()
    {
        $data['item'] = \Auth::user();

        return view('user.my_profile', $data);
    }

    public function updateMyProfile(Request $request)
    {
        $validator = Validator::make(Input::all(), $this->rules);

        // process the login
        if ($validator->fails())
        {
            return Redirect::to('/my-profile')
                ->withErrors($validator)
                ->withInput();
        }
        else {
            $item = User::find($request->input('id'));
            $item->name = $request->input('name');

            $item->save();

            // redirect
            Session::flash('message', 'Successfully updated user!');

            return Redirect::to('/my-profile');
        }
    }

    public function profileChangePassword($id)
    {
        $data['item'] = User::find($id);

        return view('user.profile_change_password', $data);
    }

    public function profileUpdatePassword(Request $request)
    {
        $item = User::find($request->input('id'));

        $validator = Validator::make(Input::all(), $this->changePasswordRules);

        // process the login
        if ($validator->fails())
        {
            return Redirect::to('profile-change-password/' . $item->id)
                ->withErrors($validator)
                ->withInput();
        }

        $item->password = bcrypt($request->input('password'));

        $item->save();

        Session::flash('message', 'Updated password for ' .$item->name. '!');

        return Redirect::to('/my-profile');
    }

    public function userRoles($id)
    {
        \Auth::user()->authorizeRoles(['admin']);

        $data['user'] = User::find($id);
        $data['items'] = Role::orderBy('name')->get();

        return view('user.user_roles', $data);
    }

    public function updateUserRoles(Request $request)
    {
        \Auth::user()->authorizeRoles(['admin']);

        $user = User::find($request->input('id'));

        $roleIds = Input::get('role');

        if(count($roleIds) > 0)
        {
            $user->roles()->detach();

            foreach ($roleIds as $roleId)
            {
                $role = Role::find($roleId);

                $user->roles()->attach($role);
            }
        }

        Session::flash('message', 'Roles update for ' .$user->name. '!');

        return Redirect::to('/users');
    }
}
