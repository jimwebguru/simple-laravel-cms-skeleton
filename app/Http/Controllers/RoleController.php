<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use App\Image;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Session;

class RoleController extends Controller
{
    private $rules = array(
            'name'       => 'required'
        );

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        \Auth::user()->authorizeRoles(['admin']);

        $data = array();
        $data["items"] = Role::paginate(30);

        return view('role.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        \Auth::user()->authorizeRoles(['admin']);

        return view('role.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \Auth::user()->authorizeRoles(['admin']);

        $validator = Validator::make(Input::all(), $this->rules);

        // process the login
        if ($validator->fails())
        {
            return Redirect::to('roles/create')
                ->withErrors($validator)
                ->withInput();
        }
        else
        {
            // store
            $item = new Role();
            $this->saveRoleItem($item, $request);

            // redirect
            Session::flash('message', 'Successfully created Role!');

            return Redirect::to('roles');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        \Auth::user()->authorizeRoles(['admin']);

        $data['item'] = Role::find($id);

        return view('role.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        \Auth::user()->authorizeRoles(['admin']);

        $data['item'] = Role::find($id);

        return view('role.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        \Auth::user()->authorizeRoles(['admin']);

        $validator = Validator::make(Input::all(), $this->rules);

        // process the login
        if ($validator->fails())
        {
            return Redirect::to('roles/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput();
        }
        else
        {
            // store
            $item = Role::find($id);
            $this->saveRoleItem($item, $request);

            // redirect
            Session::flash('message', 'Successfully updated Role!');

            return Redirect::to('roles');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \Auth::user()->authorizeRoles(['admin']);

        $item = Role::find($id);
        $item->delete();

        // redirect
        Session::flash('message', 'Successfully deleted!');

        return Redirect::to('roles');

    }

    private function saveRoleItem($item, $request)
    {
        $item->name = $request->input('name');
        $item->description = $request->input('description');

        $item->save();
    }
}
