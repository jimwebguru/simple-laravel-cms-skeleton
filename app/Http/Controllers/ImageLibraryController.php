<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Image;
use App\CmsConfig;
use Session;
use File;

class ImageLibraryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        // Grab settings
        $image_folders = CmsConfig::where('config_type','=','image-library-path')->get()->pluck('value')->toArray();

        $data["images"] = Image::paginate(18);
        $data["image_folders"] = $image_folders;

        return view('image_library.index',$data);
    }

    public function indexFilter($dir = '')
    {
        // Grab settings
        $image_folders = CmsConfig::where('config_type','=','image-library-path')->get()->pluck('value')->toArray();

        $data["images"] = Image::where("url",'LIKE','/'.$dir.'%')->paginate(18);

        $data["image_folders"] = $image_folders;
        $data["dir"] = $dir;

        return view('image_library.index',$data);
    }

    public function browser(Request $request)
    {
        // Grab settings
        $image_folders = CmsConfig::where('config_type','=','image-library-path')->get()->pluck('value')->toArray();

        $data["images"] = Image::paginate(18);
        $data["image_folders"] = $image_folders;
        $data["CKEditorFuncNum"] = $request["CKEditorFuncNum"];
        $data["CKEditor"] = $request["CKEditor"];

        return view('image_library.browser',$data);
    }

    public function browserFilter(Request $request, $dir = '')
    {
        // Grab settings
        $image_folders = CmsConfig::where('config_type','=','image-library-path')->get()->pluck('value')->toArray();

        $data["images"] = Image::where("url",'LIKE','/'.$dir.'%')->paginate(18);

        $data["image_folders"] = $image_folders;
        $data["dir"] = $dir;
        $data["CKEditorFuncNum"] = $request["CKEditorFuncNum"];
        $data["CKEditor"] = $request["CKEditor"];

        return view('image_library.browser',$data);
    }

    public function upload(Request $request)
    {
        if($request->file('image') !== null)
        {
            $folder = $request->input('folder');

            $item = new Image();
            $item->name = $request->file('image')->getClientOriginalName();

            $fileName = time().'.'.pathinfo($item->name, PATHINFO_EXTENSION);

            $item->url = '/'.$folder.'/'.$fileName;
            $item->locked = 1;

            $request->file('image')->move(public_path() . '/'.$folder, $fileName);

            $item->save();

            Session::flash('message', 'Image has been uploaded');
        }

        return redirect($request->headers->get('referer'));
    }

    public function syncDir()
    {
        // Purge table
        Image::truncate();

        // Grab settings
        $config_settings = CmsConfig::all();

        foreach ($config_settings as $setting)
        {
            $dir_list = scandir(public_path().'/'.$setting->value);
            $accepted_file_types = ['jpg','png','gif','svg'];

            foreach ($dir_list as $file)
            {
                $file_ext = strtolower(pathinfo($file,PATHINFO_EXTENSION));

                if (in_array($file_ext, $accepted_file_types))
                {
                    $item = new Image();
                    $item->url = '/'.$setting->value.'/'.$file;
                    $item->name = $file;
                    $item->locked = 1;
                    $item->save();
                }
            }
        }

        return redirect('/image-library');
    }

    public function delete(Request $request, $id)
    {
        $image = Image::find($id);

        if($image->locked == 0)
        {
            File::delete(public_path() . $image->url);
            $image->delete();

            Session::flash('message', 'Image has been deleted');
        }
        else
        {
            Session::flash('message', 'Image is locked and cannot deleted');
        }

        return redirect($request->headers->get('referer'));
    }

    public function locking(Request $request, $id)
    {
        $image = Image::find($id);

        $image->locked = ($image->locked == 0)? $image->locked = 1: $image->locked = 0;
        $image->save();

        return redirect($request->headers->get('referer'));
    }
}