<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;
use App\Image;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Session;

class BlogController extends Controller
{
    private $rules = array(
        'title'       => 'required',
        'body'      => 'required'
    );

    public function __construct()
    {
        $this->middleware('auth',['except' => ['show','blogList']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = array();
        $data["items"] = Blog::paginate(30);

        return view('blog.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(Input::all(), $this->rules);

        // process the login
        if ($validator->fails())
        {
            return Redirect::to('blogs/create')
                ->withErrors($validator)
                ->withInput();
        }
        else
        {
            // store
            $item = new Blog();
            $item->body_image_width = 200;
            $this->saveBlogItem($item, $request);

            // redirect
            Session::flash('message', 'Successfully created blog!');

            return Redirect::to('blogs');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($url)
    {
        $blogs = Blog::where('urlAlias','=', $url)->get();

        $data['item'] = $blogs[0];

        return view('blog.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['item'] = Blog::find($id);

        return view('blog.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make(Input::all(), $this->rules);

        // process the login
        if ($validator->fails())
        {
            return Redirect::to('blogs/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput();
        }
        else
        {
            // store
            $item = Blog::find($id);
            $this->saveBlogItem($item, $request);

            // redirect
            Session::flash('message', 'Successfully updated blog!');

            return Redirect::to('blogs');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Blog::find($id);
        $item->delete();

        // redirect
        Session::flash('message', 'Successfully deleted!');

        return Redirect::to('blogs');

    }

    private function saveBlogItem($item, $request)
    {
        $item->title = $request->input('title');
        $item->urlAlias = $this->cleanUrl($item->title);

        $item->body = $request->input('body');
        $item->header = $request->input('header');
        $item->author = $request->input('author');
        $item->published_date = $request->input('published_date');
        $item->body_image_alignment = $request->input('body_image_alignment');
        $item->body_image_width = $request->input('body_image_width');
        $item->header_image_max = $request->input('header_image_max');
        $item->header_image = $request->input('header_image');
        $item->body_image = $request->input('body_image');
        $item->show_author_info = $request->input('show_author_info');
        $item->tags = $request->input('tags');
        $item->body_video = $request->input('body_video');
        $item->body_video_alignment = $request->input('body_video_alignment');

        if($request->file('header_image_upload') !== null)
        {
            $folder = "images";

            $headerImage = new Image();
            $headerImage->name = $request->file('header_image_upload')->getClientOriginalName();

            $fileName = time().'-blog-banner.'.pathinfo($headerImage->name, PATHINFO_EXTENSION);

            $headerImage->url = '/'.$folder.'/'.$fileName;
            $item->header_image = $headerImage->url;
            $headerImage->locked = 1;

            $request->file('header_image_upload')->move(public_path() . '/'.$folder, $fileName);

            $headerImage->save();
        }

        if($request->file('body_image_upload') !== null)
        {
            $folder = "images";

            $bodyImage = new Image();
            $bodyImage->name = $request->file('body_image_upload')->getClientOriginalName();

            $fileName = time().'-blog-body.'.pathinfo($bodyImage->name, PATHINFO_EXTENSION);

            $bodyImage->url = '/'.$folder.'/'.$fileName;
            $item->body_image = $bodyImage->url;
            $bodyImage->locked = 1;

            $request->file('body_image_upload')->move(public_path() . '/'.$folder, $fileName);

            $bodyImage->save();
        }

        if($request->file('thumbnail_image_upload') !== null)
        {
            $folder = "images";

            $thumbnailImage = new Image();
            $thumbnailImage->name = $request->file('thumbnail_image_upload')->getClientOriginalName();

            $fileName = time().'-blog-thumbnail.'.pathinfo($thumbnailImage->name, PATHINFO_EXTENSION);

            $thumbnailImage->url = '/'.$folder.'/'.$fileName;
            $item->thumbnail_image = $thumbnailImage->url;
            $thumbnailImage->locked = 1;

            $request->file('thumbnail_image_upload')->move(public_path() . '/'.$folder, $fileName);

            $thumbnailImage->save();
        }

        $item->save();
    }

    public function blogList(Request $request)
    {
        $tagValues = "";
        $listCondition = "";

        if($request->input('tag'))
        {
            foreach($request->input('tag') as $tag)
            {
                $listCondition .= " tags LIKE '%" . strtolower($tag)."%' OR";
                $tagValues .= $tag.",";
            }

            $last_space_position = strrpos($listCondition, ' OR');
            $listCondition = substr($listCondition, 0, $last_space_position);
        }

        $data = array();
        $data["tagValues"] = $tagValues;

        if($listCondition != "")
        {
            $data["items"] = Blog::whereRaw("$listCondition")->orderBy('published_date','desc')->paginate(15);
        }
        else
        {
            $data["items"] = Blog::orderBy('published_date','desc')->paginate(15);
        }

        return view('blog.list', $data);
    }

    function cleanURL($string)
    {
        $url = str_replace("'", '', $string);
        $url = str_replace('%20', ' ', $url);
        $url = preg_replace('~[^\\pL0-9_]+~u', '-', $url); // substitutes anything but letters, numbers and '_' with separator
        $url = trim($url, "-");
        $url = iconv("utf-8", "us-ascii//TRANSLIT", $url);  // you may opt for your own custom character map for encoding.
        $url = strtolower($url);
        $url = preg_replace('~[^-a-z0-9_]+~', '', $url); // keep only letters, numbers, '_' and separator
        return $url;
    }
}