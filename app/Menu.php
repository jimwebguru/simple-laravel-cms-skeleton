<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "menus";

    public function menuItems()
    {
        return $this->hasMany('App\MenuItem', 'menu_id','id')->orderBy('weight', 'ASC');
    }

    public function getMenuLinksHtml()
    {
        $menuLinksHtml = "";

        if($this->menuItems->count() > 0)
        {
            foreach($this->menuItems as $menuItem)
            {
                if($menuItem->auth_required == 0 || (\Auth::check() && $menuItem->auth_required == 1))
                {
                    $menuLinksHtml .= "<li>";

                    if ($menuItem->menuItems->count() > 0)
                    {
                        $menuLinksHtml .= "<a href=\"" . $menuItem->url . "\">" . $menuItem->link_text . "<span class=\"caret\"></span></a>";

                        $menuLinksHtml .= "<ul class=\"dropdown-menu\">";

                        foreach($menuItem->menuItems as $subMenuItem)
                        {
                            $menuLinksHtml .= $this->getSubMenuLinksHtml($subMenuItem);
                        }

                        $menuLinksHtml .= "</ul>";
                    } else {
                        $menuLinksHtml .= "<a href=\"" . $menuItem->url . "\">" . $menuItem->link_text . "</a>";
                    }

                    $menuLinksHtml .= "</li>";
                }
            }
        }

        return $menuLinksHtml;
    }

    private function getSubMenuLinksHtml($menuItem)
    {
        $subMenuHtml = "";

        if($menuItem->auth_required == 0 || (\Auth::check() && $menuItem->auth_required == 1))
        {
            $subMenuHtml = "<li>";

            if ($menuItem->menuItems->count() > 0) {
                $subMenuHtml .= "<a href=\"" . $menuItem->url . "\">" . $menuItem->link_text . "<span class=\"caret\"></span></a>";

                $subMenuHtml .= "<ul class=\"dropdown-menu\">";

                foreach ($menuItem->menuItems as $subMenuItem) {
                    $subMenuHtml .= $this->getSubMenuLinksHtml($subMenuItem);
                }

                $subMenuHtml .= "</ul>";
            } else {
                $subMenuHtml .= "<a href=\"" . $menuItem->url . "\">" . $menuItem->link_text . "</a>";
            }

            $subMenuHtml .= "</li>";
        }

        return $subMenuHtml;
    }
}
