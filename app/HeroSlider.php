<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HeroSlider extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "herosliders";

    public function slides()
    {
        return $this->hasMany('App\Slide', 'hero_id','id')->orderBy('weight', 'ASC');
    }
}
