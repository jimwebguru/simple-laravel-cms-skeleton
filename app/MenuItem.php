<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuItem extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "menuitems";

    private $breadCrumb;

    public function menuItems()
    {
        return $this->hasMany('App\MenuItem', 'parent_menuitem_id','id')->orderBy('weight', 'ASC');
    }

    public function getBreadCrumbHtml()
    {
        $this->breadCrumb = array();

        $this->breadCrumb[] = $this->id."|".$this->link_text;

        if($this->menu_id > 0)
        {
            $this->getBreadCrumbLinks(Menu::find($this->menu_id));
        }
        else
        {
            $this->getBreadCrumbLinks(MenuItem::find($this->parent_menuitem_id));
        }

        $this->breadCrumb = array_reverse($this->breadCrumb);

        $breadCrumbHtml = "";

        for($i = 0;$i < count($this->breadCrumb);$i++)
        {
            $linkParts = explode("|", $this->breadCrumb[$i]);

            if($i == 0)
            {
                $breadCrumbHtml .= "<a href='/menus/".$linkParts[0]."/edit'>".$linkParts[1]."</a>";
            }
            else
            {
                $breadCrumbHtml .= "<a href='/menus/edit-menuitem/".$linkParts[0]."'>".$linkParts[1]."</a>";
            }

            if($i != (count($this->breadCrumb) - 1))
            {
                $breadCrumbHtml .= " > ";
            }
        }

        return $breadCrumbHtml;
    }

    private function getBreadCrumbLinks($parent)
    {
        if($parent instanceof Menu)
        {
            $this->breadCrumb[] = $parent->id . "|" . $parent->name;
        }
        else
        {
            $this->breadCrumb[] = $parent->id . "|" . $parent->link_text;

            if($parent->menu_id > 0)
            {
                $this->getBreadCrumbLinks(Menu::find($parent->menu_id));
            }
            else
            {
                $this->getBreadCrumbLinks(MenuItem::find($parent->parent_menuitem_id));
            }
        }
    }
}
