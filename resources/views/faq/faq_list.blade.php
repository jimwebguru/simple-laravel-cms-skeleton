@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1></i>FAQs</h1>

                <div class="faq-accordion">
                    @foreach($items as $key => $value)
                        <h2>{{ $value->question }}</h2>
                        <div>
                            {!! $value->answer !!}
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $( function() {
            $( ".faq-accordion" ).accordion();
        } );
    </script>

@endsection