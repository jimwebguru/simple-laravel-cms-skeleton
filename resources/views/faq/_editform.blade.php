
<div class="form-group">
    {{ Form::label('question', 'Question') }}
    {{ Form::textarea('question', (isset($item))? null : Input::old('question'), array('class' => 'form-control', 'required' => 'required', 'rows' => '4')) }}
</div>

<div class="form-group">
    {{ Form::label('answer', 'Answer') }}
    {{ Form::textarea('answer', (isset($item))? null : Input::old('answer'), array('class' => 'form-control ckeditor', 'required' => 'required')) }}
</div>

{{ Form::submit('Save', array('class' => 'btn btn-primary')) }}