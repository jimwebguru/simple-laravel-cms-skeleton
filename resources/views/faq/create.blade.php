@extends('layouts.admin')

@section('content')

    <h1><i class="fa fa-question-circle" aria-hidden="true"></i>Create FAQ</h1>

    <!-- if there are creation errors, they will show here -->
    <div class="class=alert alert-danger">{{ HTML::ul($errors->all()) }}</div>

    {{ Form::open(array('url' => 'faqs')) }}

        @include('faq._editform')

    {{ Form::close() }}

@endsection