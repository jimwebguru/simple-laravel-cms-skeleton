@extends('layouts.admin')

@section('content')

    <h1><i class="fa fa-question-circle" aria-hidden="true"></i>Edit FAQ</h1>

    <!-- if there are creation errors, they will show here -->
    <div class="class=alert alert-danger">{{ HTML::ul($errors->all()) }}</div>

    {{ Form::model($item, array('route' => array('faqs.update', $item->id), 'method' => 'PUT')) }}

        @include('faq._editform')

    {{ Form::close() }}

@endsection