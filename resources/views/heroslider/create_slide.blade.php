@extends('layouts.admin')

@section('content')

    <h1><i class="fa fa-object-group" aria-hidden="true"></i>Create Slide</h1>

    <!-- if there are creation errors, they will show here -->
    <div class="class=alert alert-danger">{{ HTML::ul($errors->all()) }}</div>

    {{ Form::open(array('url' => 'herosliders/store-slide','method' => 'post','files' => true)) }}

        @include('heroslider._slideform')

    {{ Form::close() }}

@endsection