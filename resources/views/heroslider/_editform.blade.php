
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
<style>
    .CodeMirror { height: 100px }
</style>

<div class="form-group">
    {{ Form::label('name', 'Name') }}
    {{ Form::text('name', (isset($item))? null : Input::old('name'), array('class' => 'form-control', 'required' => 'required')) }}
</div>

<div class="form-group">
    {{ Form::label('token_name', 'Token Name') }}
    {{ Form::text('token_name', (isset($item))? null : Input::old('token_name'), array('class' => 'form-control', 'required' => 'required')) }}
</div>

<div class="row">
    <div class="col-md-6 col-sm-12">
        <div class="form-group">
            {{ Form::label('title_css', 'Title CSS (optional)') }}
            {{ Form::textarea('title_css', (isset($item))? null : Input::old('title_css'), array('class' => 'form-control codemirror', 'code-mode' => 'css')) }}
        </div>
    </div>
    <div class="col-md-6 col-sm-12">
        <div class="form-group">
            {{ Form::label('caption_css', 'Caption CSS (optional)') }}
            {{ Form::textarea('caption_css', (isset($item))? null : Input::old('caption_css'), array('class' => 'form-control codemirror', 'code-mode' => 'css')) }}
        </div>
    </div>
    <div class="col-md-6 col-sm-12">
        <div class="form-group">
            {{ Form::label('button_css', 'Button CSS (optional)') }}
            {{ Form::textarea('button_css', (isset($item))? null : Input::old('button_css'), array('class' => 'form-control codemirror', 'code-mode' => 'css')) }}
        </div>
    </div>
    <div class="col-md-6 col-sm-12">
        <div class="form-group">
            {{ Form::label('button_hover_css', 'Button Hover CSS (optional)') }}
            {{ Form::textarea('button_hover_css', (isset($item))? null : Input::old('button_hover_css'), array('class' => 'form-control codemirror', 'code-mode' => 'css')) }}
        </div>
    </div>
</div>

{{ Form::submit('Save', array('class' => 'btn btn-primary')) }}