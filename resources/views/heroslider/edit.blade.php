@extends('layouts.admin')

@section('content')

    <h1><i class="fa fa-object-group" aria-hidden="true"></i>Edit Hero Slider</h1>

    <!-- if there are creation errors, they will show here -->
    <div class="class=alert alert-danger">{{ HTML::ul($errors->all()) }}</div>

    {{ Form::model($item, array('route' => array('herosliders.update', $item->id), 'method' => 'PUT')) }}

        @include('heroslider._editform')

    {{ Form::close() }}

    @include('heroslider._slides')

@endsection