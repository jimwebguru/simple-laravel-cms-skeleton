@if(isset($item) && $item->id > 0)

    <hr/>
    <h2>Slides</h2>
    <ul class="nav nav-pills">
        <li role="presentation"><a href="{{ URL::to('herosliders/create-slide/'.$item->id) }}">Add Slide</a></li>
    </ul>

    @if(count($item->slides) > 0)
        <div class="p-grid admin-grid">
            <div class="p-col-12 p-md-2 admin-grid-header">Image</div>
            <div class="p-col-12 p-md-5 admin-grid-header">Title</div>
            <div class="p-col-12 p-md-4 admin-grid-header">Actions</div>
            <div class="p-col-12 p-md-1 last-admin-grid-header"></div>

            @foreach($item->slides as $index => $slide)
                <?php
                $rowClass = "admin-row-odd";

                if($index % 2 == 0)
                {
                    $rowClass = "admin-row-even";
                }
                ?>
                <div class="p-col-12 p-md-2 admin-grid-column {{$rowClass}}">
                    @if($slide->image != null && $slide->image != '')
                        <img src="{{$slide->image}}" style="max-width: 100%"/>
                    @endif
                </div>
                <div class="p-col-12 p-md-5 admin-grid-column {{$rowClass}}">{{ $slide->title }}</div>
                <div class="p-col-12 p-md-4 admin-grid-column {{$rowClass}}">
                    <a class="btn btn-small btn-info" href="{{ URL::to('herosliders/edit-slide/' . $slide->id) }}">Edit</a>
                    <a class="btn btn-small btn-danger" style="float: right" href="{{ URL::to('herosliders/delete-slide/' . $slide->id) }}">Remove</a>
                </div>
                <div class="p-col-12 p-md-1 last-admin-grid-column {{$rowClass}}">
                    @if($index != 0)
                        <a class="btn btn-small btn-warning" href="{{ URL::to('herosliders/slide-set-weight/' . $slide->id . '/' . ($index - 1)) }}"><i class="fa fa-arrow-up" aria-hidden="true"></i></a>
                    @endif
                    @if($index != ($item->slides->count() - 1))
                        <a class="btn btn-small btn-warning" href="{{ URL::to('herosliders/slide-set-weight/' . $slide->id) . '/' . ($index + 1) }}"><i class="fa fa-arrow-down" aria-hidden="true"></i></a>
                    @endif
                </div>
            @endforeach
        </div>

        <hr/>
        <h2>Preview</h2>
        @component('components.hero_slider',['token' => $item->token_name])
        @endcomponent
    @endif
@endif