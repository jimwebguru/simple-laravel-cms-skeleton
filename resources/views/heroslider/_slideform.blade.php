
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

{{Form::hidden('hero_id', $hero_id)}}

<div class="form-group">
    {{ Form::label('title', 'Title') }}
    {{ Form::text('title', (isset($item))? $item->title : Input::old('title'), array('class' => 'form-control', 'required' => 'required')) }}
</div>

{{ Form::label('slide_image_upload', 'Image') }}
<div class="form-group" style="margin-top: 20px">
    <div class="container-fluid">
        <div class="row" style="margin-top: 20px">
            <div class="col-md-4 col-sm-12" id="slide-image-container">
                @if(isset($item) && $item->image != null && $item->image != '')
                    <img src="{{$item->image}}" style="max-width: 100%"/>
                @endif
            </div>
            <div class="col-md-5 col-sm-12">
                {!! Form::file('slide_image_upload', array('id' => 'slide-image')) !!}
            </div>
        </div>
    </div>
</div>

<div class="form-group">
    {{ Form::label('caption', 'Caption') }}
    {{ Form::textarea('caption', (isset($item))? $item->caption : Input::old('caption'), array('class' => 'form-control ckeditor')) }}
</div>

<div class="form-group">
    {{ Form::label('url', 'Url') }}
    {{ Form::text('url', (isset($item))? $item->url : Input::old('url'), array('class' => 'form-control')) }}
</div>

<div class="form-group">
    {{ Form::label('link_text', 'Link Text') }}
    {{ Form::text('link_text', (isset($item))? $item->link_text : Input::old('link_text'), array('class' => 'form-control')) }}
</div>


<script type="text/javascript">
    var slideImageLoader = document.getElementById('slide-image');

    slideImageLoader.addEventListener('change', handleSlideImage, false);

    function handleSlideImage(e)
    {
        var reader = new FileReader();

        reader.onload = function(event)
        {
            var img = new Image();
            img.src = event.target.result;
            img.style.maxWidth = "100%";

            $("#slide-image-container").empty();
            $("#slide-image-container").append(img);
        }

        reader.readAsDataURL(e.target.files[0]);
    }
</script>

{{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
<a class="btn btn-small btn-warning" href="{{ URL::to('herosliders/' . $hero_id . '/edit') }}">Cancel</a>