@extends('layouts.admin')

@section('content')

    <h1><i class="fa fa-object-group" aria-hidden="true"></i>Edit Hero Slider</h1>

    <!-- if there are creation errors, they will show here -->
    <div class="class=alert alert-danger">{{ HTML::ul($errors->all()) }}</div>

    {{ Form::open(array('url' => 'herosliders/update-slide','method' => 'post','files' => true)) }}

        {{Form::hidden('id', $item->id)}}

        @include('heroslider._slideform')

    {{ Form::close() }}

@endsection