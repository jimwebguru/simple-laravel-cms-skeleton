@extends('layouts.app')

@section('title', $content->name)

@section('content')

    <div class="container-fluid page-content" style="min-height: 300px">
        <div class="row" style="margin-top: 50px">
            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                @if(Auth::check())
                    <a class="btn btn-primary" href="/contents/{{$content->id}}/edit" style="position: absolute;right: 0">Edit</a>
                @endif

                <h1>{{$content->name}}</h1>
                {!! $content->body !!}

                @if(isset($blocks) && count($blocks) > 0)
                    @foreach($blocks as $block)
                        {!! $block->body !!}
                    @endforeach
                @endif
            </div>
            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
            </div>
        </div>
    </div>

@endsection