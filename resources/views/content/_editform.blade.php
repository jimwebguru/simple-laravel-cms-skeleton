
<div class="form-group">
    {{ Form::label('name', 'Name') }}
    {{ Form::text('name', (isset($item))? null : Input::old('name'), array('class' => 'form-control', 'required' => 'required')) }}
</div>

<div class="form-group">
    {{ Form::label('url_path', 'Url Path') }}
    {{ Form::text('url_path', (isset($item))? null : Input::old('url_path'), array('class' => 'form-control')) }}
</div>

<div class="form-group">
    {{ Form::label('block_url', 'Url of Page to append as Block') }}
    {{ Form::text('block_url', (isset($item))? null : Input::old('block_url'), array('class' => 'form-control')) }}
</div>

<div class="form-group">
    {{ Form::label('is_code', 'Is HTML Code?') }}
    {{ Form::select('is_code', array('0' => 'No', '1' => 'Yes'), (isset($item))? null : Input::old('is_code'), array('class' => 'form-control', 'onchange' => 'swapBodyControl($(this))')) }}
</div>

<div class="form-group">
    {{ Form::label('body', 'Body') }}
    @if(isset($item) && $item->is_code == 1)
        {{ Form::textarea('body', (isset($item))? null : Input::old('body'), array('class' => 'form-control codemirror')) }}
    @else
        {{ Form::textarea('body', (isset($item))? null : Input::old('body'), array('class' => 'form-control ckeditor')) }}
    @endif
</div>

<script type="text/javascript">

    function swapBodyControl(isCodeDD)
    {
        if(isCodeDD.val() == 1)
        {
            if (CKEDITOR.instances.body)
            {
                /* destroying instance */
                CKEDITOR.instances.body.destroy();
            }

            CodeMirror.fromTextArea($("#body")[0], {
                lineNumbers: true,
                mode: "htmlmixed",
                theme: "monokai",
                styleActiveLine: true,
                matchBrackets: true,
                extraKeys: {
                    "Alt-F": "findPersistent",
                    "F11": function(cm) {
                        cm.setOption("fullScreen", !cm.getOption("fullScreen"));
                    },
                    "Esc": function(cm) {
                        if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
                    }
                }
            });
        }
        else
        {
            //var CM = document.getElementById('body');
            $("div.CodeMirror").remove();

            CKEDITOR.replace('body');
        }
    }
</script>

{{ Form::submit('Save', array('class' => 'btn btn-primary')) }}