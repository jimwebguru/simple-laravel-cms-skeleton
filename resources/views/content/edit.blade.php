@extends('layouts.admin')

@section('content')

    <h1><i class="fa fa-newspaper-o" aria-hidden="true"></i>Edit Content</h1>

    <!-- if there are creation errors, they will show here -->
    <div class="class=alert alert-danger">{{ HTML::ul($errors->all()) }}</div>

    {{ Form::model($item, array('route' => array('contents.update', $item->id), 'method' => 'PUT')) }}

        @include('content._editform')

    {{ Form::close() }}

@endsection