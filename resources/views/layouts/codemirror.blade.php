<!-- Code Mirror -->
<link rel="stylesheet" href="/codemirror/lib/codemirror.css">
<link rel="stylesheet" href="/codemirror/theme/monokai.css">
<link rel="stylesheet" href="/codemirror/addon/dialog/dialog.css">
<link rel="stylesheet" href="/codemirror/addon/display/fullscreen.css">

<script src="/codemirror/lib/codemirror.js"></script>
<script src="/codemirror/mode/javascript/javascript.js"></script>
<script src="/codemirror/mode/htmlmixed/htmlmixed.js"></script>
<script src="/codemirror/mode/css/css.js"></script>
<script src="/codemirror/mode/xml/xml.js"></script>
<script src="/codemirror/addon/dialog/dialog.js"></script>
<script src="/codemirror/addon/selection/active-line.js"></script>
<script src="/codemirror/addon/edit/matchbrackets.js"></script>
<script src="/codemirror/addon/search/searchcursor.js"></script>
<script src="/codemirror/addon/search/search.js"></script>
<script src="/codemirror/addon/search/jump-to-line.js"></script>
<script src="/codemirror/addon/display/fullscreen.js"></script>

<script>
    $(document).ready(function(){
        $(".codemirror").each(function()
        {
            var editorMode = "htmlmixed";

            var attr = $(this).attr('code-mode');

            if(typeof attr !== typeof undefined && attr !== false)
            {
                editorMode = attr;
            }

            CodeMirror.fromTextArea($(this)[0], {
                lineNumbers: true,
                mode: editorMode,
                theme: "monokai",
                styleActiveLine: true,
                matchBrackets: true,
                extraKeys: {
                    "Alt-F": "findPersistent",
                    "F11": function(cm) {
                        cm.setOption("fullScreen", !cm.getOption("fullScreen"));
                    },
                    "Esc": function(cm) {
                        if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
                    }
                }
            });
        });
    });
</script>
<style>
    .CodeMirror { height: 500px }
</style>