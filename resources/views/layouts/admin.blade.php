<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('layouts.head')

    @include('layouts.codemirror')

    <script type="text/javascript" src="/ckeditor/ckeditor.js" defer="defer"></script>

    <title>@yield('title')</title>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container" style="height: 80px">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <img src="/images/generic_company.gif" style="width: 140px"/>
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="/my-profile">My Profile</a></li>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container">
            <div class="p-grid">
                <div class="p-col-12 p-md-2">
                    <h4>Sections</h4>
                    <ul class="nav nav-pills nav-stacked admin-nav">
                        <li role="presentation"><a href="/blogs"><i class="fa fa-newspaper-o" aria-hidden="true"></i>Blogs</a></li>
                        <li role="presentation"><a href="/contents"><i class="fa fa-newspaper-o" aria-hidden="true"></i>Content</a></li>
                        <li role="presentation"><a href="/herosliders"><i class="fa fa-object-group" aria-hidden="true"></i>Hero Sliders</a></li>
                        <li role="presentation"><a href="/image-library"><i class="fa fa-image" aria-hidden="true"></i>Image Library</a></li>
                        <li role="presentation"><a href="/menus"><i class="fa fa-list-alt" aria-hidden="true"></i>Menus</a></li>
                        <li role="presentation"><a href="/faqs"><i class="fa fa-question-circle" aria-hidden="true"></i>FAQs</a></li>
                        @if(Auth::user()->hasRole(['admin']))
                        <li role="presentation"><a href="/users"><i class="fa fa-user" aria-hidden="true"></i>Users</a></li>
                            <li role="presentation"><a href="/roles"><i class="fa fa-user-md" aria-hidden="true"></i>User Roles</a></li>
                        <li role="presentation"><a href="/settings"><i class="fa fa-gear" aria-hidden="true"></i>Settings</a></li>
                        @endif
                    </ul>
                    <h4>Examples</h4>
                    <ul class="nav nav-pills nav-stacked examples-nav">
                        <li role="presentation"><a href="/example/bxslider">BxSlider</a></li>
                        <li role="presentation"><a href="/example/expanding-grid">Expanding Grid</a></li>
                    </ul>
                </div>
                <div class="p-col-12 p-md-10" style="min-height: 500px">
                    @yield('content')
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row" style="height: 100px;background: #CCCCCC;margin-top: 15px">
                &nbsp;
            </div>
        </div>
    </div>


</body>
</html>
