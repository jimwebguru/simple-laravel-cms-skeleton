<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<!-- Styles -->
<link rel="stylesheet"  href="/css/jquery.bxslider.min.css"/>
<link rel="stylesheet"  href="/css/font-awesome.min.css"/>
<link rel="stylesheet" href="/jquery-ui/jquery-ui.min.css"/>
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<link rel="stylesheet" href="/css/jquery.smartmenus.bootstrap.css">

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<script type="text/javascript" src="/js/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="/js/expandingimagegrid.js"></script>
<script type="text/javascript" src="/js/jquery.smartmenus.min.js"></script>
<script type="text/javascript" src="/js/jquery.smartmenus.bootstrap.min.js"></script>
<script type="text/javascript" src="/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="/js/script.js"></script>