<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('layouts.head')
    <title>@yield('title')</title>
</head>
<body>
    <div id="app">
        <div class="container">
            <div class="p-grid">
                <div class="p-col-12 p-sm-4 p-md-4">
                    <a href="/"><img src="/images/generic_company.gif" style="max-width: 100%"/></a>
                </div>
                <div class="p-col-12 p-sm-8 p-md-8">
                    @component('components.menu',['token' => 'main-menu'])
                    @endcomponent
                </div>
            </div>
        </div>

        @yield('content')

        <div id="footer" class="container-fluid">
            <div class="p-grid">
                <div class="p-col-12">
                    <a href="#">Test Link 1</a>
                    <a href="#">Test Link 2</a>
                    <a href="#">Test Link 3</a>
                    <a href="#">Test Link 4</a>
                    <a href="#" target="_blank">Donate</a>
                    <img style="width: 100px" src="/images/seal.jpg"/>
                </div>
                <div class="p-col-12">
                    <div class="social-media-links">
                        <a href="#" title="" target="_blank" ><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a class="twitter" title="" target="_blank" href="#" ><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <a class="youtube" title="" target="_blank" href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a>
                        <a class="instagram" title="" target="_blank" href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                        <a class="flickr" title="" target="_blank" href="#"><i class="fa fa-flickr" aria-hidden="true"></i></a>
                    </div>
                </div>
                <div class="p-col-12 copyright">
                    Copyright © Test Company, All Rights Reserved. Test Company is a tax-exempt organization, and all contributions are tax-deductible according to IRS regulation.
                </div>
            </div>
        </div>
    </div>
</body>
</html>
