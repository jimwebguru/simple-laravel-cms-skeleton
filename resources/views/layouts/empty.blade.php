<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('layouts.head')
    <title>@yield('title')</title>
</head>
<body>
    <div id="app">

        @yield('content')

    </div>
</body>
</html>
