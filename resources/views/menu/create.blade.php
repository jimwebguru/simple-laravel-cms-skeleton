@extends('layouts.admin')

@section('content')

    <h1><i class="fa fa-list-alt" aria-hidden="true"></i>Create Menu</h1>

    <!-- if there are creation errors, they will show here -->
    <div class="class=alert alert-danger">{{ HTML::ul($errors->all()) }}</div>

    {{ Form::open(array('url' => 'menus')) }}

        @include('menu._editform')

    {{ Form::close() }}

@endsection