@extends('layouts.admin')

@section('content')

    <h1><i class="fa fa-list-alt" aria-hidden="true"></i>Create Menu Item</h1>

    <!-- if there are creation errors, they will show here -->
    <div class="class=alert alert-danger">{{ HTML::ul($errors->all()) }}</div>

    {{ Form::open(array('url' => 'menus/store-menuitem','method' => 'post','files' => true)) }}

        @include('menu._menuitemform')

    {{ Form::close() }}

@endsection