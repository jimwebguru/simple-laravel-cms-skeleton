@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
<style>
    .CodeMirror { height: 100px }
</style>
<div class="form-group">
    {{ Form::label('name', 'Name') }}
    {{ Form::text('name', (isset($item))? null : Input::old('name'), array('class' => 'form-control', 'required' => 'required')) }}
</div>

<div class="form-group">
    {{ Form::label('token_name', 'Token Name') }}
    {{ Form::text('token_name', (isset($item))? null : Input::old('token_name'), array('class' => 'form-control', 'required' => 'required')) }}
</div>

<div class="form-group">
    {{ Form::label('aligned_right', 'Aligned Right?') }}
    {{ Form::select('aligned_right', array('0' => 'No', '1' => 'Yes'), (isset($item))? $item->aligned_right : Input::old('auth_required'), array('class' => 'form-control')) }}
</div>

<div class="form-group">
    {{ Form::label('menu_css', 'Menu CSS (optional)') }}
    {{ Form::textarea('menu_css', (isset($item))? null : Input::old('menu_css'), array('class' => 'form-control codemirror', 'code-mode' => 'css')) }}
</div>

{{ Form::submit('Save', array('class' => 'btn btn-primary')) }}

@if(isset($item) && $item->menuItems->count() > 0)
    <a class="btn btn-info" href="#" onclick="$('#menu-preview-iframe').attr('src','/menu/preview/{{$item->id}}');return false;" data-toggle="modal" data-target="#exampleModal">Preview</a>
@endif