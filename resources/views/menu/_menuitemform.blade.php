
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

@if(isset($item))
    {!! $item->getBreadCrumbHtml() !!}
@endif

@if(isset($menu_id) || (isset($item) && $item->menu_id > 0))
    {{Form::hidden('menu_id', ((isset($menu_id))? $menu_id : $item->menu_id))}}
    {{Form::hidden('parent_menuitem_id', 0)}}
@else
    {{Form::hidden('parent_menuitem_id', ((isset($parent_menuitem_id))? $parent_menuitem_id : $item->parent_menuitem_id))}}
    {{Form::hidden('menu_id', 0)}}
@endif

<div class="form-group">
    {{ Form::label('link_text', 'Link Text') }}
    {{ Form::text('link_text', (isset($item))? $item->link_text : Input::old('link_text'), array('class' => 'form-control')) }}
</div>

<div class="form-group">
    {{ Form::label('url', 'Url') }}
    {{ Form::text('url', (isset($item))? $item->url : Input::old('url'), array('class' => 'form-control')) }}
</div>

<div class="form-group">
    {{ Form::label('auth_required', 'Auth Required?') }}
    {{ Form::select('auth_required', array('0' => 'No', '1' => 'Yes'), (isset($item))? $item->auth_required : Input::old('auth_required'), array('class' => 'form-control')) }}
</div>

{{ Form::submit('Save', array('class' => 'btn btn-primary')) }}

@if(isset($menu_id) || (isset($item) && $item->menu_id > 0))
    <a class="btn btn-small btn-warning" href="{{ URL::to('menus/' . ((isset($menu_id))? $menu_id : $item->menu_id) . '/edit') }}">Cancel</a>
@else
    <a class="btn btn-small btn-warning" href="{{ URL::to('menus/edit-menuitem/' . ((isset($parent_menuitem_id))? $parent_menuitem_id : $item->parent_menuitem_id)) }}">Cancel</a>
@endif