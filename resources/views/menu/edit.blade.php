@extends('layouts.admin')

@section('content')

    <h1><i class="fa fa-list-alt" aria-hidden="true"></i>Edit Menu</h1>

    <!-- if there are creation errors, they will show here -->
    <div class="class=alert alert-danger">{{ HTML::ul($errors->all()) }}</div>

    {{ Form::model($item, array('route' => array('menus.update', $item->id), 'method' => 'PUT')) }}

        @include('menu._editform')

    {{ Form::close() }}

    @include('menu._menuitems')

    @if(isset($item) && $item->menuItems->count() > 0)
        <div id="exampleModal" class="modal fade">
            <div class="modal-dialog" style="width: 450px">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">Menu Preview</h4>
                    </div>
                    <div class="modal-body" style="background-color: #ddd">
                        <iframe id="menu-preview-iframe" src="/menu/preview/{{$item->id}}" style="border: none;height: 500px;width: 400px"></iframe>
                    </div>
                    <div class="modal-footer">
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    @endif

@endsection