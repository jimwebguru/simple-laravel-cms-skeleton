@extends('layouts.admin')

@section('content')

    <h1><i class="fa fa-list-alt" aria-hidden="true"></i>Edit Menu Item</h1>

    <!-- if there are creation errors, they will show here -->
    <div class="class=alert alert-danger">{{ HTML::ul($errors->all()) }}</div>

    {{ Form::open(array('url' => 'menus/update-menuitem','method' => 'post','files' => true)) }}

        {{Form::hidden('id', $item->id)}}

        @include('menu._menuitemform')

    {{ Form::close() }}

    @include('menu._menuitems')

@endsection