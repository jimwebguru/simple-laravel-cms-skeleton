@if(isset($item) && $item->id > 0)
    <hr/>
    <h2>Menu Items</h2>
    <ul class="nav nav-pills">
        <li role="presentation"><a href="{{ URL::to('menus/create-menuitem/'.$item->id.(($item instanceof \App\Menu)?'/0':'/1')) }}">Add Menu Item</a></li>
    </ul>
    <style>
        .sortable-list {
            width: 100%;
            padding-bottom: 100px;
        }
        .portlet {
            margin: 0 1em 1em 0;
            padding: 0.3em;
        }
        .portlet-header {
            padding: 0.2em 0.3em;
            margin-bottom: 0.5em;
            position: relative;
        }
        .portlet-toggle {
            position: absolute;
            cursor: pointer;
            top: 50%;
            right: 0;
            margin-top: -8px;
        }
        .portlet-content {
            padding: 0.4em;
        }
        .portlet-placeholder {
            border: 1px dotted black;
            margin: 0 1em 1em 0;
            height: 50px;
        }
    </style>
    <script>
        $( function() {
            $( ".sortable-list" ).sortable({
                connectWith: ".sortable-list",
                handle: ".portlet-header",
                cancel: ".portlet-toggle",
                placeholder: "portlet-placeholder ui-corner-all",
                stop: function( event, ui )
                {
                    $.get( "/menus/menuitem-set-weight/" + ui.item.attr('portlet-id') + "/" + ui.item.index());
                }
            });

            $( ".portlet" )
                .addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
                .find( ".portlet-header" )
                .addClass( "ui-widget-header ui-corner-all" )
                .prepend( "<span class='ui-icon ui-icon-minusthick portlet-toggle'></span>");

            $( ".portlet-toggle" ).on( "click", function() {
                var icon = $( this );
                icon.toggleClass( "ui-icon-minusthick ui-icon-plusthick" );
                icon.closest( ".portlet" ).find( ".portlet-content" ).toggle();
            });
        } );
    </script>
    @if(count($item->menuItems) > 0)
        <div class="sortable-list">
            @foreach($item->menuItems as $index => $menuItem)
                <div class="portlet" portlet-id="{{$menuItem->id}}">
                    <div class="portlet-header">{{$menuItem->link_text}}</div>
                    <div class="portlet-content">
                        {{$menuItem->url}}
                        <a class="btn btn-small btn-danger" href="{{ URL::to('menus/delete-menuitem/' . $menuItem->id) }}" style="float: right;color: white;padding: 2px 8px;margin-left: 5px">Remove</a>
                        <a class="btn btn-small btn-info" href="{{ URL::to('menus/edit-menuitem/' . $menuItem->id) }}" style="float: right;color: white;padding: 2px 8px;">Edit</a>
                    </div>
                </div>
            @endforeach
        </div>
    @endif
@endif