{{ Form::open(array('url' => '/image-library/upload','files' => true)) }}

<div class="form-group" style="margin-top: 20px">
    <div class="container-fluid">
        <div class="row">
            @isset ($image_folders)
                <div class="col-md-12">
                    <label for="folder">Upload Directory:</label>
                    <select name="folder" id="folder" class="form-control">
                        @foreach($image_folders as $folder)
                            <option value="{{ $folder }}">{{ str_replace('/public', '', $folder) }}</option>
                        @endforeach
                    </select>
                </div>
            @endisset
        </div>
        <div class="row" style="margin-top: 20px">
            <div class="col-md-2" style="max-width: 140px" id="image-container">
            </div>
            <div class="col-md-5">
                {!! Form::file('image', array('id' => 'image')) !!}
            </div>
        </div>
    </div>
</div>

{!! Form::submit('Upload', array('class' => 'btn btn-primary')) !!}

{{ Form::close() }}

<script type="text/javascript">

    var imageLoader = document.getElementById('image');

    imageLoader.addEventListener('change', handleImage, false);

    function handleImage(e)
    {
        var reader = new FileReader();

        reader.onload = function(event)
        {
            var img = new Image();
            img.src = event.target.result;
            img.style.maxWidth = "100%";

            $("#image-container").empty();
            $("#image-container").append(img);
        }

        reader.readAsDataURL(e.target.files[0]);
    }
</script>