<script type="text/javascript">
    $(document).ready(function(){
        $(".image-list img").each(function(){
            if($(this).width() <  $(this).height())
            {
                $(this).addClass("portrait");
            }
        });
    });
</script>

<div id="exampleModal" class="modal fade">
    <div class="modal-dialog" style="width: 400px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Image Preview</h4>
            </div>
            <div class="modal-body" style="background-color: #ddd">
                <img id="image-preview" src="" style="max-width: 100%" />
            </div>
            <div class="modal-footer">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->