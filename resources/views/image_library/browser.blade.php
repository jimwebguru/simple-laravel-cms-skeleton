@extends('layouts.empty')

@section('content')

    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif

    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#image-list">Images</a></li>
        <li><a data-toggle="tab" href="#upload">Upload</a></li>
    </ul>

    <div class="tab-content">
        <div id="image-list" class="tab-pane fade in active">
            {{ Form::open(array('url' => '/image-browser/filter','id' => 'dir-filter')) }}
            <div class="form-group" style="margin-top: 20px">
                <div class="container-fluid">
                    <div class="row">
                        @isset ($image_folders)
                            <div class="col-md-8"></div>
                            <div class="col-md-4">
                                <label for="folder">Directory Filter:</label>
                                <select name="folder" id="folder" class="form-control" onchange="if($(this).val() != ''){$('#dir-filter').attr('action', $('#dir-filter').attr('action') + '/' + $(this).val() + '?CKEditorFuncNum={{$CKEditorFuncNum}}&CKEditor={{$CKEditor}}');$('#dir-filter').submit()}else{window.location = '/image-browser?CKEditorFuncNum={{$CKEditorFuncNum}}&CKEditor={{$CKEditor}}'}">
                                    <option value=""></option>
                                    @foreach($image_folders as $folder)
                                        <option value="{{ $folder }}" @if(isset($dir) && $dir == $folder)selected="selected"@endif>{{ str_replace('/public', '', $folder) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        @endisset
                    </div>
                </div>
            </div>
            {{ Form::close() }}

            <div class="container-fluid">
                <div class="row image-list" style="margin-top: 10px">
                    @foreach ($images as $image)
                        <div class="col-xs-2 thumbnail" style="margin-left: 5px">
                            <div onclick="insertIntoCkEditor('{{$image->url}}')" style="cursor: pointer">
                                <img src="{!!$image->url!!}" alt="{!! pathinfo($image->url,PATHINFO_FILENAME) !!}" />
                            </div>
                            <a class="download" href="{{$image->url}}" download="{{$image->name}}">
                                <i class="fa fa-download" aria-hidden="true"></i>
                            </a>
                            <a class="view" href="#" onclick="$('#image-preview').attr('src', '{{$image->url}}');return false;" data-toggle="modal" data-target="#exampleModal">
                                <i class="fa fa-eye" aria-hidden="true"></i>
                            </a>
                            @if($image->locked == 0)
                                <a class="delete" href="/image-library/delete/{{$image->id}}">
                                    <i class="fa fa-times-circle" aria-hidden="true"></i>
                                </a>
                            @endif
                            @if($image->locked == 1)
                                <a class="lock" href="/image-library/locking/{{$image->id}}">
                                    <i class="fa fa-lock" aria-hidden="true"></i>
                                </a>
                            @endif
                            @if($image->locked == 0)
                                <a class="unlock" href="/image-library/locking/{{$image->id}}">
                                    <i class="fa fa-unlock" aria-hidden="true"></i>
                                </a>
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>
            <div style="text-align: center">
                {{$images->appends(request()->except('page'))->links()}}
            </div>
        </div>
        <div id="upload" class="tab-pane fade">
            @include('image_library.upload')
        </div>
    </div>

    @include('image_library._thumbnail_script')

    <script type="text/javascript">
        function insertIntoCkEditor(fileUrl)
        {
            window.opener.CKEDITOR.tools.callFunction( '{{$CKEditorFuncNum}}', fileUrl );
            window.close();
        }
    </script>



@endsection