<?php $heroSlider = \App\HeroSlider::where('token_name','=', $token)->first(); ?>

@if(isset($heroSlider->id) && $heroSlider->id > 0)
<style>
    div.hero-slider-{{$heroSlider->id}} a.slide-link
    {
        {{$heroSlider->button_css}}
    }

    div.hero-slider-{{$heroSlider->id}} a.slide-link:hover,
    div.hero-slider-{{$heroSlider->id}} a.slide-link:active
    {
        {{$heroSlider->button_hover_css}}
    }
</style>
<div class="hero-slider hero-slider-{{$heroSlider->id}}">
    @foreach($heroSlider->slides as $slide)
        <div class="slide">
            @if($slide->image != null && $slide->image != '')
                <img src="{{$slide->image}}"/>

                <div class="slide-contents">
                    <div class="slide-title" style="{{$heroSlider->title_css}}">{{$slide->title}}</div>
                    <div class="slide-caption" style="{{$heroSlider->caption_css}}">{!! $slide->caption !!}</div>
                    <a class="slide-link" href="{{$slide->url}}">{{$slide->link_text}}</a>
                </div>
            @else
                <div class="slide-contents-relative">
                    <div class="slide-title" style="{{$heroSlider->title_css}}">{{$slide->title}}</div>
                    <div class="slide-caption" style="{{$heroSlider->caption_css}}">{!! $slide->caption !!}</div>
                    <a class="slide-link" href="{{$slide->url}}">{{$slide->link_text}}</a>
                </div>
            @endif

        </div>
    @endforeach
</div>
@endif