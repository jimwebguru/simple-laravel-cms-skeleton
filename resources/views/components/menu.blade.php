<?php $menu = \App\Menu::where('token_name','=', $token)->first(); ?>

@if(isset($menu->id) && $menu->id > 0)
<nav class="navbar navbar-default" role="navigation" style="{{$menu->menu_css}}">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse">
        <ul class="nav navbar-nav {{($menu->aligned_right == 1)?'navbar-right':''}}" data-sm-options="{collapsibleBehavior: 'link'}">
            {!! $menu->getMenuLinksHtml() !!}
        </ul>
    </div>
</nav>
@endif