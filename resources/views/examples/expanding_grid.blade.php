@extends('layouts.app')

@section('title','Skeleton Site Welcome')

@section('content')

    @include('layouts.codemirror')

    <style>
        .expanding-grid .links > li a
        {
            color: #333333;
            padding: 25px;
        }

        .img-placeholder
        {
            color: #333333;
            padding: 50px;
        }
    </style>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Expanding Grid Example</h2>
                <p>
                    You will need to change the styles in the resources/assets/sass/_expandingimagegrid.scss and compile using npm run dev in order to change css styles.
                </p>
                <div class="expanding-grid">
                    <ul class="links">
                        <li><a href="#section1">1</a></li>
                        <li><a href="#section2">2</a></li>
                        <li><a href="#section3">3</a></li>
                        <li><a href="#section4">4</a></li>
                        <li><a href="#section5">5</a></li>
                        <li><a href="#section6">6</a></li>
                        <li><a href="#section7">7</a></li>
                        <li><a href="#section8">8</a></li>
                        <li><a href="#section9">9</a></li>
                        <li><a href="#section10">10</a></li>
                        <li><a href="#section11">11</a></li>
                        <li><a href="#section12">12</a></li>
                    </ul>

                    <div id="section1" class="expanding-container">
                        <article class="hentry">
                            <h1 class="entry-title">Title</h1>
                            <div class="entry-image"><div class="img-placeholder">1</div></div>
                            <p>Description. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum consequatur, culpa voluptate distinctio iure! Error saepe cumque molestiae deserunt nemo autem non amet, aliquam vitae nulla sit praesentium unde iusto.</p>
                        </article>
                    </div>

                    <div id="section2" class="expanding-container">
                        <article class="hentry">
                            <h1 class="entry-title">Title</h1>
                            <div class="entry-image"><div class="img-placeholder">2</div></div>
                            <p>Description. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati veniam aliquam eos eius blanditiis, facilis minus quod nostrum. Dolores recusandae doloremque quam consequatur consequuntur accusantium quos possimus inventore ratione reiciendis!</p>
                        </article>
                    </div>

                    <div id="section3" class="expanding-container">
                        <article class="hentry">
                            <h1 class="entry-title">Title</h1>
                            <div class="entry-image"><div class="img-placeholder">3</div></div>
                            <p>Description. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum consequatur, culpa voluptate distinctio iure! Error saepe cumque molestiae deserunt nemo autem non amet, aliquam vitae nulla sit praesentium unde iusto.</p>
                        </article>
                    </div>

                    <div id="section4" class="expanding-container">
                        <article class="hentry">
                            <h1 class="entry-title">Title</h1>
                            <div class="entry-image"><div class="img-placeholder">4</div></div>
                            <p>Description. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati veniam aliquam eos eius blanditiis, facilis minus quod nostrum. Dolores recusandae doloremque quam consequatur consequuntur accusantium quos possimus inventore ratione reiciendis!</p>
                        </article>
                    </div>

                    <div id="section5" class="expanding-container">
                        <article class="hentry">
                            <h1 class="entry-title">Title</h1>
                            <div class="entry-image"><div class="img-placeholder">5</div></div>
                            <p>Description. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum consequatur, culpa voluptate distinctio iure! Error saepe cumque molestiae deserunt nemo autem non amet, aliquam vitae nulla sit praesentium unde iusto.</p>
                        </article>
                    </div>

                    <div id="section6" class="expanding-container">
                        <article class="hentry">
                            <h1 class="entry-title">Title</h1>
                            <div class="entry-image"><div class="img-placeholder">6</div></div>
                            <p>Description. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati veniam aliquam eos eius blanditiis, facilis minus quod nostrum. Dolores recusandae doloremque quam consequatur consequuntur accusantium quos possimus inventore ratione reiciendis!</p>
                        </article>
                    </div>

                    <div id="section7" class="expanding-container">
                        <article class="hentry">
                            <h1 class="entry-title">Title</h1>
                            <div class="entry-image"><div class="img-placeholder">7</div></div>
                            <p>Description. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum consequatur, culpa voluptate distinctio iure! Error saepe cumque molestiae deserunt nemo autem non amet, aliquam vitae nulla sit praesentium unde iusto.</p>
                        </article>
                    </div>

                    <div id="section8" class="expanding-container">
                        <article class="hentry">
                            <h1 class="entry-title">Title</h1>
                            <div class="entry-image"><div class="img-placeholder">8</div></div>
                            <p>Description. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati veniam aliquam eos eius blanditiis, facilis minus quod nostrum. Dolores recusandae doloremque quam consequatur consequuntur accusantium quos possimus inventore ratione reiciendis!</p>
                        </article>
                    </div>

                    <div id="section9" class="expanding-container">
                        <article class="hentry">
                            <h1 class="entry-title">Title</h1>
                            <div class="entry-image"><div class="img-placeholder">9</div></div>
                            <p>Description. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum consequatur, culpa voluptate distinctio iure! Error saepe cumque molestiae deserunt nemo autem non amet, aliquam vitae nulla sit praesentium unde iusto.</p>
                        </article>
                    </div>

                    <div id="section10" class="expanding-container">
                        <article class="hentry">
                            <h1 class="entry-title">Title</h1>
                            <div class="entry-image"><div class="img-placeholder">10</div></div>
                            <p>Description. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati veniam aliquam eos eius blanditiis, facilis minus quod nostrum. Dolores recusandae doloremque quam consequatur consequuntur accusantium quos possimus inventore ratione reiciendis!</p>
                        </article>
                    </div>

                    <div id="section11" class="expanding-container">
                        <article class="hentry">
                            <h1 class="entry-title">Title</h1>
                            <div class="entry-image"><div class="img-placeholder">11</div></div>
                            <p>Description. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum consequatur, culpa voluptate distinctio iure! Error saepe cumque molestiae deserunt nemo autem non amet, aliquam vitae nulla sit praesentium unde iusto.</p>
                        </article>
                    </div>

                    <div id="section12" class="expanding-container">
                        <article class="hentry">
                            <h1 class="entry-title">Title</h1>
                            <div class="entry-image"><div class="img-placeholder">12</div></div>
                            <p>Description. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati veniam aliquam eos eius blanditiis, facilis minus quod nostrum. Dolores recusandae doloremque quam consequatur consequuntur accusantium quos possimus inventore ratione reiciendis!</p>
                        </article>
                    </div>
                </div>

                <h2>HTML Code</h2>

                <textarea class="codemirror">
                    <div class="expanding-grid">
                        <ul class="links">
                            <li><a href="#section1">1</a></li>
                            <li><a href="#section2">2</a></li>
                            <li><a href="#section3">3</a></li>
                            <li><a href="#section4">4</a></li>
                            <li><a href="#section5">5</a></li>
                            <li><a href="#section6">6</a></li>
                            <li><a href="#section7">7</a></li>
                            <li><a href="#section8">8</a></li>
                            <li><a href="#section9">9</a></li>
                            <li><a href="#section10">10</a></li>
                            <li><a href="#section11">11</a></li>
                            <li><a href="#section12">12</a></li>
                        </ul>

                        <div id="section1" class="expanding-container">
                            <article class="hentry">
                                <h1 class="entry-title">Title</h1>
                                <div class="entry-image"><div class="img-placeholder">1</div></div>
                                <p>Description. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum consequatur, culpa voluptate distinctio iure! Error saepe cumque molestiae deserunt nemo autem non amet, aliquam vitae nulla sit praesentium unde iusto.</p>
                            </article>
                        </div>

                        <div id="section2" class="expanding-container">
                            <article class="hentry">
                                <h1 class="entry-title">Title</h1>
                                <div class="entry-image"><div class="img-placeholder">2</div></div>
                                <p>Description. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati veniam aliquam eos eius blanditiis, facilis minus quod nostrum. Dolores recusandae doloremque quam consequatur consequuntur accusantium quos possimus inventore ratione reiciendis!</p>
                            </article>
                        </div>

                        <div id="section3" class="expanding-container">
                            <article class="hentry">
                                <h1 class="entry-title">Title</h1>
                                <div class="entry-image"><div class="img-placeholder">3</div></div>
                                <p>Description. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum consequatur, culpa voluptate distinctio iure! Error saepe cumque molestiae deserunt nemo autem non amet, aliquam vitae nulla sit praesentium unde iusto.</p>
                            </article>
                        </div>

                        <div id="section4" class="expanding-container">
                            <article class="hentry">
                                <h1 class="entry-title">Title</h1>
                                <div class="entry-image"><div class="img-placeholder">4</div></div>
                                <p>Description. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati veniam aliquam eos eius blanditiis, facilis minus quod nostrum. Dolores recusandae doloremque quam consequatur consequuntur accusantium quos possimus inventore ratione reiciendis!</p>
                            </article>
                        </div>

                        <div id="section5" class="expanding-container">
                            <article class="hentry">
                                <h1 class="entry-title">Title</h1>
                                <div class="entry-image"><div class="img-placeholder">5</div></div>
                                <p>Description. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum consequatur, culpa voluptate distinctio iure! Error saepe cumque molestiae deserunt nemo autem non amet, aliquam vitae nulla sit praesentium unde iusto.</p>
                            </article>
                        </div>

                        <div id="section6" class="expanding-container">
                            <article class="hentry">
                                <h1 class="entry-title">Title</h1>
                                <div class="entry-image"><div class="img-placeholder">6</div></div>
                                <p>Description. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati veniam aliquam eos eius blanditiis, facilis minus quod nostrum. Dolores recusandae doloremque quam consequatur consequuntur accusantium quos possimus inventore ratione reiciendis!</p>
                            </article>
                        </div>

                        <div id="section7" class="expanding-container">
                            <article class="hentry">
                                <h1 class="entry-title">Title</h1>
                                <div class="entry-image"><div class="img-placeholder">7</div></div>
                                <p>Description. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum consequatur, culpa voluptate distinctio iure! Error saepe cumque molestiae deserunt nemo autem non amet, aliquam vitae nulla sit praesentium unde iusto.</p>
                            </article>
                        </div>

                        <div id="section8" class="expanding-container">
                            <article class="hentry">
                                <h1 class="entry-title">Title</h1>
                                <div class="entry-image"><div class="img-placeholder">8</div></div>
                                <p>Description. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati veniam aliquam eos eius blanditiis, facilis minus quod nostrum. Dolores recusandae doloremque quam consequatur consequuntur accusantium quos possimus inventore ratione reiciendis!</p>
                            </article>
                        </div>

                        <div id="section9" class="expanding-container">
                            <article class="hentry">
                                <h1 class="entry-title">Title</h1>
                                <div class="entry-image"><div class="img-placeholder">9</div></div>
                                <p>Description. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum consequatur, culpa voluptate distinctio iure! Error saepe cumque molestiae deserunt nemo autem non amet, aliquam vitae nulla sit praesentium unde iusto.</p>
                            </article>
                        </div>

                        <div id="section10" class="expanding-container">
                            <article class="hentry">
                                <h1 class="entry-title">Title</h1>
                                <div class="entry-image"><div class="img-placeholder">10</div></div>
                                <p>Description. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati veniam aliquam eos eius blanditiis, facilis minus quod nostrum. Dolores recusandae doloremque quam consequatur consequuntur accusantium quos possimus inventore ratione reiciendis!</p>
                            </article>
                        </div>

                        <div id="section11" class="expanding-container">
                            <article class="hentry">
                                <h1 class="entry-title">Title</h1>
                                <div class="entry-image"><div class="img-placeholder">11</div></div>
                                <p>Description. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum consequatur, culpa voluptate distinctio iure! Error saepe cumque molestiae deserunt nemo autem non amet, aliquam vitae nulla sit praesentium unde iusto.</p>
                            </article>
                        </div>

                        <div id="section12" class="expanding-container">
                            <article class="hentry">
                                <h1 class="entry-title">Title</h1>
                                <div class="entry-image"><div class="img-placeholder">12</div></div>
                                <p>Description. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati veniam aliquam eos eius blanditiis, facilis minus quod nostrum. Dolores recusandae doloremque quam consequatur consequuntur accusantium quos possimus inventore ratione reiciendis!</p>
                            </article>
                        </div>
                    </div>
                </textarea>
            </div>
        </div>
    </div>

@endsection
