@extends('layouts.app')

@section('title','Skeleton Site Welcome')

@section('content')

    @include('layouts.codemirror')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>BxSlider Example</h2>
                <p>
                    You will need to add styles in the resources/assets/sass/app.scss and compile using npm run dev in order to change css styles.
                </p>
                <p>
                    Reference <a href="https://bxslider.com/options/">https://bxslider.com/options/</a> for more information.
                </p>
                <div class="hero-slider">
                    <div class="slide">
                        <img src="/images/placeholder-img.jpg"/>
                        <div class="slide-contents">
                            <div class="slide-title">Home page slider 1</div>
                            <div class="slide-caption"><p>Donec non lacus a odio pellentesque tristique. Donec dignissim neque purus, id fringilla tellus tincidunt eu. Nulla facilisi. Aliquam lacinia dapibus nisi et ultrices.</p></div>
                            <a class="slide-link" href="#">Read More</a>
                        </div>
                    </div>
                    <div class="slide">
                        <img src="/images/placeholder-img.jpg"/>
                        <div class="slide-contents">
                            <div class="slide-title">Home page slider 2</div>
                            <div class="slide-caption"><p>Donec non lacus a odio pellentesque tristique. Donec dignissim neque purus, id fringilla tellus tincidunt eu. Nulla facilisi. Aliquam lacinia dapibus nisi et ultrices.</p></div>
                            <a class="slide-link" href="#">Read More</a>
                        </div>
                    </div>
                    <div class="slide">
                        <img src="/images/placeholder-img.jpg"/>
                        <div class="slide-contents">
                            <div class="slide-title">Home page slider 3</div>
                            <div class="slide-caption"><p>Donec non lacus a odio pellentesque tristique. Donec dignissim neque purus, id fringilla tellus tincidunt eu. Nulla facilisi. Aliquam lacinia dapibus nisi et ultrices.</p></div>
                            <a class="slide-link" href="#">Read More</a>
                        </div>
                    </div>
                    <div class="slide">
                        <img src="/images/placeholder-img.jpg"/>
                        <div class="slide-contents">
                            <div class="slide-title">Home page slider 4</div>
                            <div class="slide-caption"><p>Donec non lacus a odio pellentesque tristique. Donec dignissim neque purus, id fringilla tellus tincidunt eu. Nulla facilisi. Aliquam lacinia dapibus nisi et ultrices.</p></div>
                            <a class="slide-link" href="#">Read More</a>
                        </div>
                    </div>
                </div>

                <script>
                    $(document).ready(function(){
                        $('.hero-slider').bxSlider({auto: true, pause: 6000, stopAutoOnClick: true, responsive: true});
                    });
                </script>

                <h2>HTML Code</h2>

                <textarea class="codemirror">
                    <div class="hero-slider">
                        <div class="slide">
                            <img src="/images/placeholder-img.jpg"/>
                            <span class="slide-title">Home page slider 1</span>
                            <span class="slide-caption"> Donec non lacus a odio pellentesque tristique. Donec dignissim neque purus, id fringilla tellus tincidunt eu. Nulla facilisi. Aliquam lacinia dapibus nisi et ultrices. </span>
                            <a class="slide-link" href="#">Read More</a>
                        </div>
                        <div class="slide">
                            <img src="/images/placeholder-img.jpg"/>
                            <span class="slide-title">Home page slider 2</span>
                            <span class="slide-caption"> Donec non lacus a odio pellentesque tristique. Donec dignissim neque purus, id fringilla tellus tincidunt eu. Nulla facilisi. Aliquam lacinia dapibus nisi et ultrices. </span>
                            <a class="slide-link" href="#">Read More</a>
                        </div>
                        <div class="slide">
                            <img src="/images/placeholder-img.jpg"/>
                            <span class="slide-title">Home page slider 3</span>
                            <span class="slide-caption"> Donec non lacus a odio pellentesque tristique. Donec dignissim neque purus, id fringilla tellus tincidunt eu. Nulla facilisi. Aliquam lacinia dapibus nisi et ultrices. </span>
                            <a class="slide-link" href="#">Read More</a>
                        </div>
                        <div class="slide">
                            <img src="/images/placeholder-img.jpg"/>
                            <span class="slide-title">Home page slider 4</span>
                            <span class="slide-caption"> Donec non lacus a odio pellentesque tristique. Donec dignissim neque purus, id fringilla tellus tincidunt eu. Nulla facilisi. Aliquam lacinia dapibus nisi et ultrices. </span>
                            <a class="slide-link" href="#">Read More</a>
                        </div>
                    </div>
                </textarea>

                <h2>Javascript Code</h2>

                <textarea class="codemirror">
                    <script>
                        $(document).ready(function(){
                            $('.hero-slider').bxSlider({auto: true, pause: 6000, stopAutoOnClick: true, responsive: true});
                        });
                    </script>
                </textarea>
            </div>
        </div>
    </div>

@endsection
