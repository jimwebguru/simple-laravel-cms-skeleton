@extends('layouts.admin')

@section('content')

    <h1><i class="fa fa-user" aria-hidden="true"></i>Change Password for {{$item->name}}</h1>

    <!-- if there are creation errors, they will show here -->
    <div class="class=alert alert-danger">{{ HTML::ul($errors->all()) }}</div>

    {{ Form::open(array('url' => 'update-password')) }}

        @include('user._passwordform')

        {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

@endsection