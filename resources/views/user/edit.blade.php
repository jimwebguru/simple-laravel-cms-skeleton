@extends('layouts.admin')

@section('content')

    <h1><i class="fa fa-user" aria-hidden="true"></i>Edit User</h1>

    <!-- if there are creation errors, they will show here -->
    <div class="class=alert alert-danger">{{ HTML::ul($errors->all()) }}</div>

    {{ Form::model($item, array('route' => array('users.update', $item->id), 'method' => 'PUT')) }}

        @include('user._editform')

        {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

@endsection