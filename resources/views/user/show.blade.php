@extends('layouts.admin')

@section('content')

    <h1>Content {{ $item->name }}</h1>

    <div class="jumbotron text-center">
        <h2>{{ $item->name }}</h2>
        <p>
            <strong>Url Path:</strong> {{ $item->url_path }}<br>
            <strong>Body:</strong> {{ $item->body }}
        </p>
    </div>

@endsection