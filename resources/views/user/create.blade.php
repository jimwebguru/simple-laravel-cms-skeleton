@extends('layouts.admin')

@section('content')

    <h1><i class="fa fa-user" aria-hidden="true"></i>Create User</h1>

    <!-- if there are creation errors, they will show here -->
    <div class="class=alert alert-danger">{{ HTML::ul($errors->all()) }}</div>

    {{ Form::open(array('url' => 'users')) }}

        @include('user._editform')

        <div class="form-group">
            {{ Form::label('password', 'Password') }}
            {{ Form::password('password', array('class' => 'form-control')) }}
        </div>

        {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

@endsection