{{Form::hidden('id', $item->id)}}

<div class="form-group">
    {{ Form::label('password', 'New Password') }}
    {{ Form::password('password', array('class' => 'form-control', 'required' => 'required')) }}
</div>

<div class="form-group">
    {{ Form::label('password_confirmation', 'Confirm New Password') }}
    {{ Form::password('password_confirmation', array('class' => 'form-control', 'required' => 'required')) }}
</div>

