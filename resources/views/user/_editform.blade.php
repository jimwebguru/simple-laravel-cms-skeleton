
<div class="form-group">
    {{ Form::label('name', 'Name') }}
    {{ Form::text('name', (isset($item))? null : Input::old('name'), array('class' => 'form-control', 'required' => 'required')) }}
</div>

<div class="form-group">
    {{ Form::label('email', 'Email') }}
    {{ Form::text('email', (isset($item))? null : Input::old('email'), array('class' => 'form-control')) }}
</div>

