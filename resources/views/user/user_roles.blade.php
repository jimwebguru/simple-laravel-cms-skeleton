@extends('layouts.admin')

@section('content')

    <h1><i class="fa fa-user-md" aria-hidden="true"></i>Update roles for user: {{$user->name}}</h1>

    <p>Select the roles you would like to add to user {{$user->name}}.</p>
    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif

    {{ Form::open(array('url' => 'update-user-roles')) }}

    {{Form::hidden('id', $user->id)}}

    @foreach($items as $key => $value)
        <?php
            $checked = "";

            if($user->hasRole($value->name))
            {
                $checked = "checked='checked'";
            }
        ?>
        <input name="role[]" value="{{$value->id}}" type="checkbox" {{$checked}}/> {{$value->name}}<br/>
    @endforeach

    {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

@endsection