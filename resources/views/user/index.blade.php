@extends('layouts.admin')

@section('content')

    <ul class="nav nav-pills">
        <li role="presentation"><a href="{{ URL::to('/users/create') }}">Create User</a></li>
    </ul>
    <h1><i class="fa fa-user" aria-hidden="true"></i>Users</h1>

    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif

    <div class="p-grid admin-grid">
        <div class="p-col-12 p-md-2 p-lg-3 p-xl-4 admin-grid-header">Name</div>
        <div class="p-col-12 p-md-4 p-lg-4 admin-grid-header">Email</div>
        <div class="p-col-12 p-md-6 p-lg-5 p-xl-4 last-admin-grid-header">Actions</div>

        @foreach($items as $key => $value)
            <?php
            $rowClass = "admin-row-odd";

            if($key % 2 == 0)
            {
                $rowClass = "admin-row-even";
            }
            ?>
            <div class="p-col-12 p-md-2 p-lg-3 p-xl-4 admin-grid-column {{$rowClass}}">{{ $value->name }}</div>
            <div class="p-col-12 p-md-4 p-lg-4 admin-grid-column {{$rowClass}}">{{ $value->email }}</div>
            <div class="p-col-12 p-md-6 p-lg-5 p-xl-4 last-admin-grid-column {{$rowClass}}">
                <a class="btn btn-small btn-info" href="{{ URL::to('users/' . $value->id . '/edit') }}">Edit</a>
                <a class="btn btn-small btn-warning" href="{{ URL::to('change-password/' . $value->id) }}">Change Password</a>

                @if(!$value->hasRole('admin'))
                    {{ Form::open(array('url' => 'users/' . $value->id, 'class' => 'pull-right')) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                    {{ Form::close() }}
                @endif
            </div>
        @endforeach
    </div>

@endsection