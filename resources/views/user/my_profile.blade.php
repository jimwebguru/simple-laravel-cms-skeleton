@extends('layouts.admin')

@section('content')

    <h1><i class="fa fa-user" aria-hidden="true"></i>My Profile</h1>

    <!-- if there are creation errors, they will show here -->
    <div class="class=alert alert-danger">{{ HTML::ul($errors->all()) }}</div>

    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif

    {{ Form::model($item, array('url' => '/update-my-profile', 'method' => 'POST')) }}

        {{Form::hidden('id')}}
        {{Form::hidden('email')}}

        <div class="form-group">
            {{ Form::label('name', 'Name') }}
            {{ Form::text('name', (isset($item))? null : Input::old('name'), array('class' => 'form-control', 'required' => 'required')) }}
        </div>

        <div class="form-group">
            <label>Email</label><br/>
            {{$item->email}}
        </div>

        <div class="form-group">
            <label>Roles</label><br/>
            <?php
            $roleNames = "";

            foreach($item->roles as $role)
            {
                $roleNames .= $role->name.", ";
            }

            $roleNames = trim($roleNames, ", ");
            ?>
            {{$roleNames}}
        </div>

        {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
        <a class="btn btn-small btn-warning" href="{{ URL::to('/profile-change-password/' . $item->id) }}">Change Password</a>

    {{ Form::close() }}

@endsection