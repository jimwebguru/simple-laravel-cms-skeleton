@extends('layouts.app')

@section('title','Skeleton Site Welcome')

@section('content')

    <div class="container">
        <div class="p-grid">
            <div class="p-col-12">
                <div class="hero-slider">
                    <div class="slide">
                        <img src="/images/placeholder-img.jpg"/>
                        <div class="slide-contents">
                            <div class="slide-title">Home page slider 1</div>
                            <div class="slide-caption"><p>Donec non lacus a odio pellentesque tristique. Donec dignissim neque purus, id fringilla tellus tincidunt eu. Nulla facilisi. Aliquam lacinia dapibus nisi et ultrices.</p></div>
                            <a class="slide-link" href="#">Read More</a>
                        </div>
                    </div>
                    <div class="slide">
                        <img src="/images/placeholder-img.jpg"/>
                        <div class="slide-contents">
                            <div class="slide-title">Home page slider 2</div>
                            <div class="slide-caption"><p>Donec non lacus a odio pellentesque tristique. Donec dignissim neque purus, id fringilla tellus tincidunt eu. Nulla facilisi. Aliquam lacinia dapibus nisi et ultrices.</p></div>
                            <a class="slide-link" href="#">Read More</a>
                        </div>
                    </div>
                    <div class="slide">
                        <img src="/images/placeholder-img.jpg"/>
                        <div class="slide-contents">
                            <div class="slide-title">Home page slider 3</div>
                            <div class="slide-caption"><p>Donec non lacus a odio pellentesque tristique. Donec dignissim neque purus, id fringilla tellus tincidunt eu. Nulla facilisi. Aliquam lacinia dapibus nisi et ultrices.</p></div>
                            <a class="slide-link" href="#">Read More</a>
                        </div>
                    </div>
                    <div class="slide">
                        <img src="/images/placeholder-img.jpg"/>
                        <div class="slide-contents">
                            <div class="slide-title">Home page slider 4</div>
                            <div class="slide-caption"><p>Donec non lacus a odio pellentesque tristique. Donec dignissim neque purus, id fringilla tellus tincidunt eu. Nulla facilisi. Aliquam lacinia dapibus nisi et ultrices.</p></div>
                            <a class="slide-link" href="#">Read More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
        $content1 = \App\Content::find(1);
        $content2 = \App\Content::find(2);
        $content3 = \App\Content::find(3);
    ?>
    <div class="container">
        <div class="p-grid">
            <div class="p-col-12 p-md-4">
                <h2>{{$content1->name}}</h2>
                {!! $content1->body !!}
            </div>
            <div class="p-col-12 p-md-4">
                <h2>{{$content2->name}}</h2>
                {!! $content2->body !!}
            </div>
            <div class="p-col-12 p-md-4">
                <h2>{{$content3->name}}</h2>
                {!! $content3->body !!}
            </div>
        </div>
    </div>

@endsection
