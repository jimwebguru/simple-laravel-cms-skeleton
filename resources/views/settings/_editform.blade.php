<div class="form-group">
    {{ Form::label('config_type', 'Config Type') }}
    {{ Form::select('config_type', array('image-library-path' => 'Images Library Path', 'blog-tag' => 'Blog Tag'), (isset($item))? null : Input::old('config_type'), array('class' => 'form-control')) }}
</div>

<div class="form-group">
    {{ Form::label('value', 'Value') }}
    {{ Form::text('value', (isset($item))? null : Input::old('value'), array('class' => 'form-control')) }}
</div>

<div class="form-group">
    {{ Form::label('locked', 'Locked?') }}
    {{ Form::select('locked', array('0' => 'No', '1' => 'Yes'), (isset($item))? null : Input::old('locked'), array('class' => 'form-control')) }}
</div>

{{ Form::submit('Save', array('class' => 'btn btn-primary')) }}