@extends('layouts.admin')

@section('content')

    <ul class="nav nav-pills">
        <li role="presentation"><a href="{{ URL::to('settings/create') }}">Create Setting</a></li>
    </ul>
    <h1><i class="fa fa-gear" aria-hidden="true"></i>Settings</h1>

    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif

    <div class="p-grid admin-grid">
        <div class="p-col-12 p-md-1 admin-grid-header">Id</div>
        <div class="p-col-12 p-md-4 p-xl-5 admin-grid-header">Config Type</div>
        <div class="p-col-12 p-md-3 admin-grid-header">Value</div>
        <div class="p-col-12 p-md-4 p-xl-3 last-admin-grid-header">Actions</div>

        @foreach($items as $key => $value)
            <?php
            $rowClass = "admin-row-odd";

            if($key % 2 == 0)
            {
                $rowClass = "admin-row-even";
            }
            ?>
            <div class="p-col-12 p-md-1 admin-grid-column {{$rowClass}}">{{ $value->id }}</div>
            <div class="p-col-12 p-md-4 p-xl-5 admin-grid-column {{$rowClass}}">{{ $value->config_type }}</div>
            <div class="p-col-12 p-md-3 admin-grid-column {{$rowClass}}">{{$value->value}}</div>
            <div class="p-col-12 p-md-4 p-xl-3 last-admin-grid-column {{$rowClass}}">
                <a class="btn btn-small btn-info" href="{{ URL::to('settings/' . $value->id . '/edit') }}">Edit</a>

                @if($value->locked == 0)
                    {{ Form::open(array('url' => 'settings/' . $value->id, 'class' => 'pull-right')) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                    {{ Form::close() }}
                @endif
            </div>
        @endforeach
    </div>

@endsection