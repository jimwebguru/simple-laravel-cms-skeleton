@extends('layouts.admin')

@section('content')

    <h1><i class="fa fa-gear" aria-hidden="true"></i>Edit Setting</h1>

    <!-- if there are creation errors, they will show here -->
    <div class="class=alert alert-danger">{{ HTML::ul($errors->all()) }}</div>

    {{ Form::model($item, array('route' => array('settings.update', $item->id), 'method' => 'PUT')) }}

        @include('settings._editform')

    {{ Form::close() }}

@endsection