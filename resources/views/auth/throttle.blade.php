@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading" style="text-align: center"><img src="/images/generic_company.gif" style="max-width: 200px"/></div>
                        <div class="panel-body">
                            <div class="alert alert-danger">
                                    {{$message}}
                            </div>
                            <div>
                                <a href="{{url('/')}}">Click Here</a> to go back to home screen.
                            </div>
                        </div>
                </div>
           </div>
        </div>
    </div>
@endsection