<div class="form-group">
    {{ Form::label('title', 'Title') }}
    {{ Form::text('title', (isset($item))? null : Input::old('title'), array('class' => 'form-control', 'required' => 'required')) }}
</div>

<hr/>

{{ Form::label('thumbnail_image_upload', 'Thumbnail Image') }}
<div class="form-group" style="margin-top: 20px">
    <div class="container-fluid">
        <div class="row" style="margin-top: 20px">
            <div class="col-md-2" style="max-width: 140px" id="thumbnail-image-container">
                @if(isset($item) && $item->thumbnail_image != null && $item->thumbnail_image != '')
                    <img src="{{$item->thumbnail_image}}" style="max-width: 100%"/>
                @endif
            </div>
            <div class="col-md-5">
                {!! Form::file('thumbnail_image_upload', array('id' => 'thumbnail-image')) !!}
                <div class="alert alert-danger" role="alert" id="thumbnail-image-error" style="display: none">That file size is too large. Please select a different image at 300KB or smaller.</div>
            </div>
        </div>
    </div>
</div>

<hr/>

{{ Form::label('header_image_upload', 'Header Image') }}
<div class="form-group" style="margin-top: 20px">
    <div class="container-fluid">
        <div class="row" style="margin-top: 20px">
            <div class="col-md-2" style="max-width: 140px" id="header-image-container">
                @if(isset($item) && $item->header_image != null && $item->header_image != '')
                    <img src="{{$item->header_image}}" style="max-width: 100%"/>
                @endif
            </div>
            <div class="col-md-5">
                {!! Form::file('header_image_upload', array('id' => 'header-image')) !!}
                <div class="alert alert-danger" role="alert" id="header-image-error" style="display: none">That file size is too large. Please select a different image at 300KB or smaller.</div>
            </div>
        </div>
    </div>
</div>

<div class="form-group">
    {{ Form::label('header_image', 'Header Image Url ( if not uploading )') }}
    {{ Form::text('header_image', (isset($item))? null : Input::old('header_image'), array('class' => 'form-control')) }}
</div>

<div class="form-group">
    {{ Form::label('header_image_max', 'Header Image Max Width?') }}
    {{ Form::select('header_image_max', array('0' => 'No', '1' => 'Yes'), (isset($item))? null : Input::old('header_image_max'), array('class' => 'form-control')) }}
</div>

<div class="form-group">
    {{ Form::label('header', 'Header') }}
    {{ Form::textarea('header', (isset($item))? null : Input::old('header'), array('class' => 'form-control ckeditor')) }}
</div>

<hr/>

{{ Form::label('body_image_upload', 'Body Image') }}
<div class="form-group" style="margin-top: 20px">
    <div class="container-fluid">
        <div class="row" style="margin-top: 20px">
            <div class="col-md-2" style="max-width: 140px" id="body-image-container">
                @if(isset($item) && $item->body_image != null && $item->body_image != '')
                    <img src="{{$item->body_image}}" style="max-width: 100%"/>
                @endif
            </div>
            <div class="col-md-5">
                {!! Form::file('body_image_upload', array('id' => 'body-image')) !!}
                <div class="alert alert-danger" role="alert" id="body-image-error" style="display: none">That file size is too large. Please select a different image at 300KB or smaller.</div>
            </div>
        </div>
    </div>
</div>

<div class="form-group">
    {{ Form::label('body_image', 'Body Image Url ( if not uploading )') }}
    {{ Form::text('body_image', (isset($item))? null : Input::old('body_image'), array('class' => 'form-control')) }}
</div>

<div class="form-group">
    {{ Form::label('body_image_width', 'Body Image Width') }}
    {{ Form::number('body_image_width', (isset($item))? null : Input::old('body_image_width'), array('class' => 'form-control')) }}
</div>

<div class="form-group">
    {{ Form::label('body_image_alignment', 'Body Image Alignment') }}
    {{ Form::select('body_image_alignment', array('0' => 'Left', '1' => 'Right'), (isset($item))? null : Input::old('body_image_alignment'), array('class' => 'form-control')) }}
</div>

<div class="form-group">
    {{ Form::label('body_video', 'Body Video Url ( Video will only show up if there is no body image. Use video embed url. )') }}
    {{ Form::text('body_video', (isset($item))? null : Input::old('body_video'), array('class' => 'form-control')) }}
</div>

<div class="form-group">
    {{ Form::label('body_video_alignment', 'Body Video Alignment') }}
    {{ Form::select('body_video_alignment', array('0' => 'Left', '1' => 'Right'), (isset($item))? null : Input::old('body_video_alignment'), array('class' => 'form-control')) }}
</div>

<div class="form-group">
    {{ Form::label('body', 'Body') }}
    {{ Form::textarea('body', (isset($item))? null : Input::old('body'), array('class' => 'form-control ckeditor')) }}
</div>

<hr/>

<?php
$blogTags = \App\CmsConfig::where('config_type', '=', 'blog-tag')->get();
?>

@if($blogTags->count() > 0)
<h2>Tags</h2>

{{ Form::hidden('tags', (isset($item))? null : Input::old('tags'), array("id" => "tags")) }}

@foreach($blogTags as $bTag)
<div class="form-check">
    <input class="form-check-input" type="checkbox" value="{{strtolower($bTag->value)}}" name="tag[]" id="tag{{$bTag->id}}"/>
    <label class="form-check-label" for="tag{{$bTag->id}}">
        {{$bTag->value}}
    </label>
</div>
@endforeach

<hr/>
@endif

<h2>Author Info</h2>
<div class="form-group">
    {{ Form::label('author', 'Author') }}
    {{ Form::text('author', (isset($item))? null : Input::old('author'), array('class' => 'form-control')) }}
</div>

<div class="form-group">
    {{ Form::label('published_date', 'Published Date') }}
    {{ Form::date('published_date', (isset($item))? $item->published_date : Input::old('published_date'), array('class' => 'form-control', 'required' => 'required')) }}
</div>

<div class="form-group">
    {{ Form::label('show_author_info', 'Show Author Info?') }}
    {{ Form::select('show_author_info', array('0' => 'No','1' => 'Yes'), (isset($item))? null : Input::old('show_author_info'), array('class' => 'form-control')) }}
</div>

<script type="text/javascript">
    $(document).ready(function()
    {
        if($("#tags").val() != '')
        {
            $("input[name='tag[]']").each(function()
            {
                if($("#tags").val().indexOf($(this).val()) >= 0)
                {
                    $(this).click();
                }
            });
        }

        function populateTagsElement()
        {
            var tagValue = "";

            $("input[name='tag[]']").each(function()
            {
                if($(this).is(':checked'))
                {
                    tagValue += $(this).val() + ",";
                }
            });

            $("#tags").val(tagValue);
        }

        $("input[name='tag[]']").change(function()
        {
            populateTagsElement();
        });
    });
</script>

<script type="text/javascript">

    var headerImageLoader = document.getElementById('header-image');

    headerImageLoader.addEventListener('change', handleHeaderImage, false);

    function handleHeaderImage(e)
    {
        var reader = new FileReader();

        reader.onload = function(event)
        {
            var img = new Image();
            img.src = event.target.result;
            img.style.maxWidth = "100%";

            if(document.getElementById('header-image').files[0].size > 300000)
            {
                $('#header-image').val('');
                $('#header-image-error').show();
            }
            else
            {
                $('#header-image-error').hide();
                $("#header-image-container").empty();
                $("#header-image-container").append(img);
            }
        }

        reader.readAsDataURL(e.target.files[0]);
    }

    var bodyImageLoader = document.getElementById('body-image');

    bodyImageLoader.addEventListener('change', handleBodyImage, false);

    function handleBodyImage(e)
    {
        var reader = new FileReader();

        reader.onload = function(event)
        {
            var img = new Image();
            img.src = event.target.result;
            img.style.maxWidth = "100%";

            if(document.getElementById('body-image').files[0].size > 300000)
            {
                $('#body-image').val('');
                $('#body-image-error').show();
            }
            else
            {
                $('#body-image-error').hide();
                $("#body-image-container").empty();
                $("#body-image-container").append(img);
            }
        }

        reader.readAsDataURL(e.target.files[0]);
    }

    var thumbnailImageLoader = document.getElementById('thumbnail-image');

    thumbnailImageLoader.addEventListener('change', handleThumbnailImage, false);

    function handleThumbnailImage(e)
    {
        var reader = new FileReader();

        reader.onload = function(event)
        {
            var img = new Image();
            img.src = event.target.result;
            img.style.maxWidth = "100%";

            if(document.getElementById('thumbnail-image').files[0].size > 300000)
            {
                $('#header-image').val('');
                $('#header-image-error').show();
            }
            else
            {
                $('#thumbnail-image-error').hide();
                $("#thumbnail-image-container").empty();
                $("#thumbnail-image-container").append(img);
            }
        }

        reader.readAsDataURL(e.target.files[0]);
    }
</script>

{{ Form::submit('Save', array('class' => 'btn btn-primary')) }}