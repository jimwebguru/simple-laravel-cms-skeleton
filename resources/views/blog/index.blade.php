@extends('layouts.admin')

@section('content')

    <ul class="nav nav-pills">
        <li role="presentation"><a href="{{ URL::to('blogs/create') }}">Create Blog</a></li>
        <li role="presentation"><a href="{{ URL::to('blog-list') }}">View Site Blog List</a></li>
    </ul>
    <h1><i class="fa fa-newspaper-o" aria-hidden="true"></i>Blogs</h1>

    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif

    <div class="p-grid admin-grid">
        <div class="p-col-12 p-md-1 admin-grid-header">Id</div>
        <div class="p-col-12 p-md-5 p-lg-6 p-xl-7 admin-grid-header">Title</div>
        <div class="p-col-12 p-md-6 p-lg-5 p-xl-4 last-admin-grid-header">Actions</div>

        @foreach($items as $key => $value)
            <?php
            $rowClass = "admin-row-odd";

            if($key % 2 == 0)
            {
                $rowClass = "admin-row-even";
            }
            ?>
            <div class="p-col-12 p-md-1 admin-grid-column {{$rowClass}}">{{ $value->id }}</div>
            <div class="p-col-12 p-md-5 p-lg-6  p-xl-7 admin-grid-column {{$rowClass}}">{{ $value->title }}</div>
            <div class="p-col-12 p-md-6 p-lg-5 p-xl-4 last-admin-grid-column {{$rowClass}}">
                <a class="btn btn-small btn-info" href="{{ URL::to('blogs/' . $value->urlAlias) }}">View</a>
                <a class="btn btn-small btn-info" href="{{ URL::to('blogs/' . $value->id . '/edit') }}">Edit</a>

                {{ Form::open(array('url' => 'blogs/' . $value->id, 'class' => 'pull-right')) }}
                {{ Form::hidden('_method', 'DELETE') }}
                {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
            </div>
        @endforeach
    </div>

    {{$items->links()}}

@endsection