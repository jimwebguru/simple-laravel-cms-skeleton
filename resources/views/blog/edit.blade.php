@extends('layouts.admin')

@section('content')

    <h1><i class="fa fa-newspaper-o" aria-hidden="true"></i>Edit Blog</h1>

    <!-- if there are creation errors, they will show here -->
    <div class="class=alert alert-danger">{{ HTML::ul($errors->all()) }}</div>

    {{ Form::model($item, array('route' => array('blogs.update', $item->id), 'method' => 'PUT','files' => true)) }}

        @include('blog._editform')

    {{ Form::close() }}

@endsection