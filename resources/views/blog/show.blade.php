@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p><a href="/blog-list">Back to Blog List</a></p>
                <h1>{{ $item->title }}</h1>

                <div>
                    <p>
                        @if($item->show_author_info == 1)
                            <b>Author:</b> {{$item->author }} <b>Date:</b> {{$item->published_date->format('m/d/Y') }}
                        @endif
                        @if(Auth::check())
                            &nbsp;<a href="/blogs/{{$item->id}}/edit">Edit</a>
                        @endif
                    </p>
                </div>

                <div>
                    @if($item->header_image != null && $item->header_image != '')
                        <?php
                            $imageWidth = "auto";

                            if($item->header_image_max == 1)
                            {
                                $imageWidth = "100%";
                            }
                        ?>
                        <img src="{{$item->header_image}}" style="width: {{$imageWidth}};margin-bottom: 10px" />
                    @endif
                    {!! $item->header !!}
                </div>

                <div>
                    @if($item->body_image != null && trim($item->body_image) != '')
                        <?php
                        $float = "blog-image-left";

                        if($item->body_image_alignment == 1)
                        {
                            $float = "blog-image-right";
                        }
                        ?>
                        <div class="{{$float}}">
                            <img src="{{$item->body_image}}" style="width: {{($item->body_image_width != 0 && $item->body_image_width != '')? $item->body_image_width.'px' : 'auto'}};max-width: 100%"/>
                        </div>
                    @elseif($item->body_video != null && trim($item->body_video) != '')
                        <?php
                        $vidfloat = "blog-image-left";
                        $marginType = "margin-right";

                        if($item->body_video_alignment == 1)
                        {
                            $vidfloat = "blog-image-right";
                            $marginType = "margin-left";
                        }
                        ?>
                        <div style="max-width: 540px;width: 100%;{{$marginType}}: 10px;" class="{{$vidfloat}}">
                            <div class="video-container"><iframe allowfullscreen="" class="video" frameborder="0" mozallowfullscreen="" src="{{$item->body_video}}" webkitallowfullscreen=""></iframe></div>
                        </div>
                    @endif
                    {!! $item->body !!}
                </div>
            </div>
        </div>
    </div>



@endsection