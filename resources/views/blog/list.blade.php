@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Blog List</h1>
            </div>
            <div class="col-md-2 blog-list-filters">
                <?php
                $blogTags = \App\CmsConfig::where('config_type', '=', 'blog-tag')->get();
                ?>

                @if($blogTags->count() > 0)
                    <h4>Filters</h4>
                    {{ Form::open(array('url' => '/blog-list', 'method' => 'get')) }}
                        @foreach($blogTags as $bTag)
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="{{strtolower($bTag->value)}}" name="tag[]" id="tag{{$bTag->id}}"/>
                            <label class="form-check-label" for="tag{{$bTag->id}}">
                                {{$bTag->value}}
                            </label>
                        </div>
                        @endforeach
                    {{ Form::submit('Filter', array('class' => 'btn btn-primary')) }}
                {{ Form::close() }}
                @endif
            </div>
            <div class="col-md-10">
                <div class="container-fluid">
                    @foreach($items as $key => $value)

                        <div class="row">
                            <div class="col-md-12" style="padding-bottom: 10px">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2">
                                            @if(isset($value->thumbnail_image) && $value->thumbnail_image != '')
                                                <a href="/blogs/{{$value->urlAlias}}"><img src="{{url($value->thumbnail_image)}}" style="max-width: 100%"/></a>
                                            @elseif(isset($value->header_image) && $value->header_image != '')
                                                <a href="/blogs/{{$value->urlAlias}}"><img src="{{url($value->header_image)}}" style="max-width: 100%"/></a>
                                            @elseif(isset($value->body_image) && $value->body_image != '')
                                                <a href="/blogs/{{$value->urlAlias}}"><img src="{{url($value->body_image)}}" style="max-width: 100%"/></a>
                                            @endif
                                        </div>
                                        <div class="col-md-10 col-sm-10">
                                            <div><h3 class="blog-list-item-header"><a href="/blogs/{{$value->urlAlias}}" style="color: #636b6f">{{$value->title}}</a></h3></div>
                                            <div>{!! strip_tags(str_limit($value->body, 350, '...')) !!}</div>
                                            <div><a href="/blogs/{{$value->urlAlias}}">Read More</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    @endforeach
                </div>

                {{$items->appends(request()->except('page'))->links()}}
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $(document).ready(function(){
            var selectedTags = "@if(isset($tagValues)) {{$tagValues}} @endif";

            $("input[name='tag[]']").each(function()
            {
                if(selectedTags.indexOf($(this).val()) >= 0)
                {
                    $(this).click();
                }
            });
        });
    </script>

@endsection