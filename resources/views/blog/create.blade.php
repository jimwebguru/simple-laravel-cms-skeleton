@extends('layouts.admin')

@section('content')

    <h1><i class="fa fa-newspaper-o" aria-hidden="true"></i>Create Blog</h1>

    <!-- if there are creation errors, they will show here -->
    <div class="class=alert alert-danger">{{ HTML::ul($errors->all()) }}</div>

    {{ Form::open(array('url' => 'blogs','files' => true)) }}

        @include('blog._editform')

    {{ Form::close() }}

@endsection