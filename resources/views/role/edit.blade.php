@extends('layouts.admin')

@section('content')

    <h1><i class="fa fa-user-md" aria-hidden="true"></i>Edit Role</h1>

    <!-- if there are creation errors, they will show here -->
    <div class="class=alert alert-danger">{{ HTML::ul($errors->all()) }}</div>

    {{ Form::model($item, array('route' => array('roles.update', $item->id), 'method' => 'PUT')) }}

        @include('role._editform')

    {{ Form::close() }}

@endsection