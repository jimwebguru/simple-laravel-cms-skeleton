
<div class="form-group">
    {{ Form::label('name', 'Name') }}
    {{ Form::text('name', (isset($item))? null : Input::old('name'), array('class' => 'form-control', 'required' => 'required')) }}
</div>

<div class="form-group">
    {{ Form::label('description', 'Description') }}
    {{ Form::textarea('description', (isset($item))? null : Input::old('description'), array('class' => 'form-control')) }}
</div>


{{ Form::submit('Save', array('class' => 'btn btn-primary')) }}