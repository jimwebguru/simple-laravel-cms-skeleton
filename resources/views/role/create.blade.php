@extends('layouts.admin')

@section('content')

    <h1><i class="fa fa-user-md" aria-hidden="true"></i>Create Role</h1>

    <!-- if there are creation errors, they will show here -->
    <div class="class=alert alert-danger">{{ HTML::ul($errors->all()) }}</div>

    {{ Form::open(array('url' => 'roles')) }}

        @include('role._editform')

    {{ Form::close() }}

@endsection