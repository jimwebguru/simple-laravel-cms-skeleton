<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/image-library', 'ImageLibraryController@index');
Route::get('/image-library/filter/{dir}', 'ImageLibraryController@indexFilter');
Route::post('/image-library/filter/{dir}', 'ImageLibraryController@indexFilter');
Route::post('/image-library/upload', 'ImageLibraryController@upload');
Route::get('/image-library/sync', 'ImageLibraryController@syncDir');
Route::get('image-library/delete/{id}','ImageLibraryController@delete');
Route::get('/image-browser', 'ImageLibraryController@browser');
Route::post('/image-browser/filter/{dir}', 'ImageLibraryController@browserFilter');
Route::get('/image-browser/filter/{dir}', 'ImageLibraryController@browserFilter');
Route::get('/image-library/locking/{id}', 'ImageLibraryController@locking');

Route::get('/blog-list','BlogController@blogList');

Route::get('/change-password/{id}', 'UserController@changePassword');
Route::post('/update-password', 'UserController@updatePassword');
Route::get('/my-profile', 'UserController@myProfile');
Route::post('/update-my-profile', 'UserController@updateMyProfile');
Route::get('/profile-change-password/{id}', 'UserController@profileChangePassword');
Route::post('/profile-update-password', 'UserController@profileUpdatePassword');
Route::get('/user-roles/{id}','UserController@userRoles');
Route::post('/update-user-roles', 'UserController@updateUserRoles');

Route::resource('blogs','BlogController');
Route::resource('users', 'UserController');
Route::resource('contents', 'ContentController');
Route::resource('settings', 'CmsConfigController');
Route::resource('roles','RoleController');
Route::resource('faqs', 'FaqController');

Route::resource('menus', 'MenuController');
Route::get('/menus/create-menuitem/{id}/{parentType}','MenuController@createMenuItem');
Route::get('/menus/edit-menuitem/{id}','MenuController@editMenuItem');
Route::get('/menus/delete-menuitem/{id}','MenuController@deleteMenuItem');
Route::post('/menus/store-menuitem','MenuController@storeMenuItem');
Route::post('/menus/update-menuitem','MenuController@updateMenuItem');
Route::get('/menus/menuitem-set-weight/{id}/{weight}','MenuController@setMenuItemWeight');
Route::get('/menu/preview/{id}','MenuController@preview');

Route::resource('herosliders','HeroSliderController');
Route::get('/herosliders/create-slide/{id}','HeroSliderController@createSlide');
Route::get('/herosliders/edit-slide/{id}','HeroSliderController@editSlide');
Route::get('/herosliders/delete-slide/{id}','HeroSliderController@deleteSlide');
Route::post('/herosliders/store-slide','HeroSliderController@storeSlide');
Route::post('/herosliders/update-slide','HeroSliderController@updateSlide');
Route::get('/herosliders/slide-set-weight/{id}/{weight}','HeroSliderController@setSlideWeight');
Route::get('/heroslider/clone/{id}', 'HeroSliderController@cloneHeroSlider');

Route::get('content/{urlAlias}','ContentController@content');
Route::get('/content/set-weight/{id}/{weight}','ContentController@setWeight');
Route::get('/content/clone/{id}', 'ContentController@cloneContent');
Route::get('/faq-list','FaqController@faqList');

/* EXAMPLES */

Route::get('/example/expanding-grid', function () {
    return view('examples.expanding_grid');
});

Route::get('/example/bxslider', function () {
    return view('examples.bxslider');
});