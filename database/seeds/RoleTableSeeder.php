<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_dev = new Role();
        $role_dev->name = "developer";
        $role_dev->description = "A web developer User";
        $role_dev->save();

        $role_employee = new Role();
        $role_employee->name = "user";
        $role_employee->description = "A User";
        $role_employee->save();

        $role_manager = new Role();
        $role_manager->name = "admin";
        $role_manager->description = "An admin User";
        $role_manager->save();

        $user = User::where('id','=', 1)->first();
        $user->roles()->attach($role_manager);
    }
}
