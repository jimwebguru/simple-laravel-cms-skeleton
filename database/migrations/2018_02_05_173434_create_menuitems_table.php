<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMenuitemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('menuitems', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->bigInteger('menu_id')->unsigned()->nullable()->default(0);
			$table->bigInteger('parent_menuitem_id')->unsigned()->nullable()->default(0);
			$table->string('url');
			$table->string('link_text', 100);
			$table->integer('weight')->nullable()->default(0);
			$table->timestamps();
			$table->boolean('auth_required')->nullable()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('menuitems');
	}

}
