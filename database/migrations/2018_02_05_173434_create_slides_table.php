<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSlidesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('slides', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->string('title', 100);
			$table->text('caption', 65535)->nullable();
			$table->string('url')->nullable();
			$table->string('link_text')->nullable();
			$table->bigInteger('hero_id')->unsigned()->nullable()->default(0);
			$table->string('image')->nullable();
			$table->timestamps();
			$table->integer('weight')->nullable()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('slides');
	}

}
