<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBlogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('blogs', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->string('header_image')->nullable();
			$table->text('body', 65535)->nullable();
			$table->text('header', 65535)->nullable();
			$table->string('author', 150)->nullable();
			$table->dateTime('published_date')->nullable();
			$table->timestamps();
			$table->string('title')->nullable();
			$table->string('body_image')->nullable();
			$table->integer('body_image_alignment')->unsigned()->nullable();
			$table->integer('body_image_width')->unsigned()->nullable()->default(200);
			$table->boolean('header_image_max')->nullable()->default(0);
			$table->boolean('show_author_info')->nullable()->default(1);
            $table->string('tags', 255)->nullable();
            $table->string('urlAlias', 300)->nullable();
            $table->string('thumbnail_image',255)->nullable();
            $table->string('body_video', 255)->nullable();
            $table->integer('body_video_alignment')->nullable()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('blogs');
	}

}
