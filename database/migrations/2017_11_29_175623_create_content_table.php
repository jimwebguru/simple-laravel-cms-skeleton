<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('content', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->text('body', 65535);
			$table->string('url_path')->nullable();
			$table->timestamps();
			$table->string('block_url')->nullable();
			$table->boolean('is_code')->nullable()->default(0);
		});

        DB::table('content')->insert(
            array(
                'name' => 'Test Content 1',
                'body' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque quis hendrerit nulla. Donec lobortis maximus dui, id porta lorem. Nulla facilisi. Vestibulum eget enim quam. Pellentesque suscipit auctor turpis nec egestas. Praesent vitae posuere est. Suspendisse placerat, lorem quis consectetur congue, tellus sapien efficitur elit, sit amet ornare ligula odio non massa. Pellentesque in lacinia sapien. Duis eu neque sed nunc aliquet luctus ac ac mi. Sed in bibendum dolor, sit amet condimentum sapien. Donec sit amet enim libero. Pellentesque mollis vestibulum aliquet. Quisque consectetur at massa sed porta. Pellentesque sed feugiat odio.</p>'
            )
        );

        DB::table('content')->insert(
            array(
                'name' => 'Test Content 2',
                'body' => '<p>Curabitur volutpat rhoncus velit id consectetur. Etiam nunc lectus, elementum tristique gravida eu, euismod id turpis. Ut in dictum orci. Suspendisse varius eu enim eu laoreet. Phasellus suscipit justo a accumsan fermentum. Morbi a malesuada purus, sed iaculis arcu. Sed bibendum nisl sed metus ullamcorper, quis tincidunt turpis fringilla. Pellentesque convallis efficitur ipsum non gravida. Donec sed nisl id elit sagittis luctus a et nisl. Phasellus bibendum a nisi sit amet lobortis. Vivamus viverra est id dignissim gravida. Donec laoreet lectus a euismod laoreet.</p>'
            )
        );

        DB::table('content')->insert(
            array(
                'name' => 'Test Content 3',
                'body' => '<p>Fusce in sem gravida, accumsan eros sed, mollis arcu. Pellentesque quis risus facilisis tortor viverra tempus. Fusce ornare eros sed faucibus aliquam. Nunc pulvinar feugiat massa ac imperdiet. In a bibendum lorem. Donec convallis tellus eget neque porta, at volutpat nulla elementum. Nulla rhoncus neque metus, ac tincidunt erat varius non. Quisque semper ante mauris, quis lobortis massa hendrerit sed. Ut volutpat, nibh sit amet dictum cursus, elit nisl mattis sapien, eu gravida est est ac felis. Nulla vulputate mi consectetur, bibendum felis eget, tempor sem. Ut varius, neque ac laoreet consectetur, justo metus ullamcorper nibh, a imperdiet sapien nisi ac erat. Ut pretium massa nibh.</p>'
            )
        );

	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('content');
	}

}
