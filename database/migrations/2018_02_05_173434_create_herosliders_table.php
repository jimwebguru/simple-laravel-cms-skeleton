<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHeroslidersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('herosliders', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->string('name', 100);
			$table->timestamps();
			$table->string('token_name', 45);
			$table->string('title_css', 1000)->nullable();
			$table->string('caption_css', 1000)->nullable();
			$table->string('button_css', 1000)->nullable();
			$table->string('button_hover_css', 1000)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('herosliders');
	}

}
